<style>
  .blog-container {
    white-space: nowrap;
    background-color: #f2f2f2;
    padding: 20spx;

  }
  
  .blog-post {
    display: inline-block;
    width: 30%;
    margin-right: 20px;
    background-color: #fff;
    font-size: 16px;
    padding: 50px;
    margin-top: 50px; 
    align-items: center;
    margin-bottom: 50px;
    margin-right: 20px;
    margin-left: 40px;
    
  }

</style>

<div class="blog-container">
  <div class="blog-post" id="blogdata">
  <div class="testimony-wrap py-4">
                     <div class="icon d-flex align-items-center justify-content-center"><span class="fa fa-quote-left"></div>
                     <div class="text">
                      
                     <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>
                      Sed sed risus euismod, ultricies felis in, vestibulum metus.<br>
                      Morbi rhoncus nulla a urna consectetur faucibus.<br> 
                      Donec a eros aliquet, faucibus quam vel, suscipit nulla.<br>
                      Integer sollicitudin elit sit amet urna interdum rutrum. </p>
                    <a href="#">Read More</a>
                             
                       <div class="d-flex align-items-center">
                    
                     <div class="pl-3">
                     
                    
                     </div>
                     </div>
                     </div>
                     </div></div>
  
 
  <div class="blog-post">
  <div class="testimony-wrap py-4">
                     <div class="icon d-flex align-items-center justify-content-center"><span class="fa fa-quote-left"></div>
                     <div class="text">
                       <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>
                      Sed sed risus euismod, ultricies felis in, vestibulum metus.<br>
                      Morbi rhoncus nulla a urna consectetur faucibus.<br> 
                      Donec a eros aliquet, faucibus quam vel, suscipit nulla.<br>
                      Integer sollicitudin elit sit amet urna interdum rutrum. </p>
                    <a href="#">Read More</a>
                     <div class="d-flex align-items-center">
                    
                     <div class="pl-3">
                     
                    
                     </div>
                     </div>
                     </div>
                     </div></div>

  <div class="blog-post">
  <div class="testimony-wrap py-4">
                     <div class="icon d-flex align-items-center justify-content-center"><span class="fa fa-quote-left"></div>
                     <div class="text">
                       <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>
                      Sed sed risus euismod, ultricies felis in, vestibulum metus.<br>
                      Morbi rhoncus nulla a urna consectetur faucibus.<br> 
                      Donec a eros aliquet, faucibus quam vel, suscipit nulla.<br>
                      Integer sollicitudin elit sit amet urna interdum rutrum. </p>
                    <a href="#">Read More</a>
                     <div class="d-flex align-items-center">
                     
                     
                     </div>
                     </div>
                     </div></div>

                     <section>
  <div class="blog-post">
  <div class="testimony-wrap py-4">
                     <div class="icon d-flex align-items-center justify-content-center"><span class="fa fa-quote-left"></div>
                     <div class="text">
                     <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>
                      Sed sed risus euismod, ultricies felis in, vestibulum metus.<br>
                      Morbi rhoncus nulla a urna consectetur faucibus.<br> 
                      Donec a eros aliquet, faucibus quam vel, suscipit nulla.<br>
                      Integer sollicitudin elit sit amet urna interdum rutrum. </p>
                    <a href="#">Read More</a>

                     <div class="d-flex align-items-center">
                    <div class="pl-3">
    

                    
                     </div>
                     </div>
                     </div>
                     </div></div>
                      <div class="blog-post">
  <div class="testimony-wrap py-4">
                     <div class="icon d-flex align-items-center justify-content-center"><span class="fa fa-quote-left"></div>
                     <div class="text">
                     <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>
                      Sed sed risus euismod, ultricies felis in, vestibulum metus.<br>
                      Morbi rhoncus nulla a urna consectetur faucibus.<br> 
                      Donec a eros aliquet, faucibus quam vel, suscipit nulla.<br>
                      Integer sollicitudin elit sit amet urna interdum rutrum. </p>
                    <a href="#">Read More</a>

                     <div class="d-flex align-items-center">
                    <div class="pl-3">
    

                    
                     </div>
                     </div>
                     </div>
                     </div></div>
                      <div class="blog-post">
  <div class="testimony-wrap py-4">
                     <div class="icon d-flex align-items-center justify-content-center"><span class="fa fa-quote-left"></div>
                     <div class="text">
                     <p class="mb-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>
                      Sed sed risus euismod, ultricies felis in, vestibulum metus.<br>
                      Morbi rhoncus nulla a urna consectetur faucibus.<br> 
                      Donec a eros aliquet, faucibus quam vel, suscipit nulla.<br>
                      Integer sollicitudin elit sit amet urna interdum rutrum. </p>
                    <a href="#">Read More</a>

                     <div class="d-flex align-items-center">
                    <div class="pl-3">
    

                    
                     </div>
                     </div>
                     </div>
                     </div></div>
                     <script>
              $(function(){ 
              // var a = {'a','b','c'};

                  const settings = {
                                "url": "https://goaccomplish.com/api/shreya/blogs.php",
                                "method": "GET",
                                "timeout": 0,
                                "headers": {
                                    "Content-Type": "application/x-www-form-urlencoded"
                                },
                                
                              };
         // console.log(settings.headers);           
                  $.ajax(settings).done(function(response) {

                    var json = JSON.parse(response);
                    var blogs = json.data;
                    var html = '';
                    var desc ='';
                    
                    blogs.forEach((blog) => {

                        desc=blog.course_desc;
                        html+='<div class="col-sm-4"><h3>'+blog.course_name+'</h3><img class="blog image" img src="'+blog.image+'"><p>'+desc.substring(0,50)+'...</p><a class="viewmore" for="'+blog.course_id+'"> View more..</a></div>';                      
                    });
                    
                   
                    $("#blogdata").html(html);
 
                  });
                  const authors = {
                                "url": "https://goaccomplish.com/api/shreya/blog_sidebar.php?a=author",
                                "method": "GET",
                                "timeout": 0,
                                "headers": {
                                    "Content-Type": "application/x-www-form-urlencoded"
                                },
                                
                              };
         // console.log(authors.headers);           
                  $.ajax(authors).done(function(response) {

                    var json = JSON.parse(response);
                    var blogs = json;
                    var html = '';
                    var desc ='';
                    
                    blogs.forEach((blog) => {

                        // desc=blog.course_desc;
                        html+='<option value="'+blog.id+'">'+blog.name+'</option>';                      
                              
                  
              });
                      
        </script>

  
 
