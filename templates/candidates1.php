<header>
      <section class="main-title-section-wrapper default" style="">
        <div class="container">
            <div class="main-title-section">
                <h1 style="margin-bottom: 50px;">Candidates</h1>
            </div>
             <div class="breadcrumb">
            </div>
        </div>
    </section>   
    </header>
    <main>
      <h2>Golden Opportunities</h2>
      <p>Welcome to Our Company Name, where we specialize in providing high-quality services to our customers. We are committed to delivering exceptional results to our clients and are proud to have a team of experienced professionals who are dedicated to meeting your needs.</p>
      <img src="templates/download.png" alt="Our Company Name" class="company-image">
      <h3>Our Mission</h3>
      <p>At Our Company Name, our mission is to provide our clients with the best possible services at competitive prices. We believe that by delivering excellent customer service and maintaining strong relationships with our clients, we can create long-term partnerships that benefit everyone involved.</p>
      <img src="mission-image.jpg" alt="Our Mission" class="mission-image">
      <h3>Our Team</h3>
      <p>Our team is made up of highly trained professionals who are experts in their fields. They are passionate about providing the best possible services to our clients and are dedicated to staying up-to-date with the latest industry trends and technologies.</p>
      <img src="templates/download.jpeg" alt="Our Team" class="team-image">
      


      <div id="main">
        <div class="vc_column-inner" style="width:50%;margin:auto;">
        <div class="wpb_wrapper">
            <div class="ult-animation  ult-animate-viewport  ult-no-mobile " data-animate="bounce" data-animation-delay="0.8" data-animation-duration="1" data-animation-iteration="1" style="opacity:1;-webkit-transition-delay: 0.8s; -moz-transition-delay: 0.8s; transition-delay: 0.8s;" data-opacity_start_effect="">
              <div class="dt-sc-title script-with-sub-title animated bounce" style="opacity:1;-webkit-animation-delay:0.8s;-webkit-animation-duration:1s;-webkit-animation-iteration-count:1; -moz-animation-delay:0.8s;-moz-animation-duration:1s;-moz-animation-iteration-count:1; animation-delay:0.8s;animation-duration:1s;animation-iteration-count:1;test">
               
                  <h5><strong></strong></h5>

              </div>
              <div role="form" class="wpcf7 animated bounce" id="wpcf7-f8442-p8439-o1" lang="en-US" dir="ltr" style="opacity:1;-webkit-animation-delay:0.8s;-webkit-animation-duration:1s;-webkit-animation-iteration-count:1; -moz-animation-delay:0.8s;-moz-animation-duration:1s;-moz-animation-iteration-count:1; animation-delay:0.8s;animation-duration:1s;animation-iteration-count:1;test">
                  <div class="screen-reader-response">
                    <ul></ul>
                  </div>
                
              </div>
              </div>
          </div>

             </main>
                    <div class="wpcf7-response-output" aria-hidden="true"></div>
                  </form>
              </div>
            </div>
        </div>
      </div>
    
    </main>
      <!-- **Main - End** -->   
           <!-- **Footer** -->
      
        