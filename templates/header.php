<!DOCTYPE html>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	  <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <script>document.documentElement.className = document.documentElement.className + ' yes-js js_active js'</script>
    <title>Gojobs &#8211;</title>
    <meta name='robots' content='max-image-preview:large' />
    <link href='https://fonts.gstatic.com' crossorigin rel='preconnect' />
    <link rel="alternate" type="application/rss+xml" 
    title="Consultancy WordPress Theme &raquo; Feed" href="https://dtconsultancy.wpengine.com/feed/" />
    <link rel="alternate" type="application/rss+xml" 
    title="Consultancy WordPress Theme &raquo; Comments Feed" href="https://dtconsultancy.wpengine.com/comments/feed/" />
	
<style type="text/css">
	img.wp-smiley,
	img.emoji {
		display: inline !important;
		border: none !important;
		box-shadow: none !important;
		height: 1em !important;
		width: 1em !important;
		margin: 0 0.07em !important;
		vertical-align: -0.1em !important;
		background: none !important;
		padding: 0 !important;
	}
</style>
		
	<style id='wp-block-library-theme-inline-css' type='text/css'>
	.wp-block-audio figcaption{color:#555;font-size:13px;text-align:center}.is-dark-theme .wp-block-audio figcaption{color:hsla(0,0%,100%,.65)}.wp-block-audio{margin:0 0 1em}.wp-block-code{border:1px solid #ccc;border-radius:4px;font-family:Menlo,Consolas,monaco,monospace;padding:.8em 1em}.wp-block-embed figcaption{color:#555;font-size:13px;text-align:center}.is-dark-theme .wp-block-embed figcaption{color:hsla(0,0%,100%,.65)}.wp-block-embed{margin:0 0 1em}.blocks-gallery-caption{color:#555;font-size:13px;text-align:center}.is-dark-theme .blocks-gallery-caption{color:hsla(0,0%,100%,.65)}.wp-block-image figcaption{color:#555;font-size:13px;text-align:center}.is-dark-theme .wp-block-image figcaption{color:hsla(0,0%,100%,.65)}.wp-block-image{margin:0 0 1em}.wp-block-pullquote{border-top:4px solid;border-bottom:4px solid;margin-bottom:1.75em;color:currentColor}.wp-block-pullquote__citation,.wp-block-pullquote cite,.wp-block-pullquote footer{color:currentColor;text-transform:uppercase;font-size:.8125em;font-style:normal}.wp-block-quote{border-left:.25em solid;margin:0 0 1.75em;padding-left:1em}.wp-block-quote cite,.wp-block-quote footer{color:currentColor;font-size:.8125em;position:relative;font-style:normal}.wp-block-quote.has-text-align-right{border-left:none;border-right:.25em solid;padding-left:0;padding-right:1em}.wp-block-quote.has-text-align-center{border:none;padding-left:0}.wp-block-quote.is-large,.wp-block-quote.is-style-large,.wp-block-quote.is-style-plain{border:none}.wp-block-search .wp-block-search__label{font-weight:700}.wp-block-search__button{border:1px solid #ccc;padding:.375em .625em}:where(.wp-block-group.has-background){padding:1.25em 2.375em}.wp-block-separator.has-css-opacity{opacity:.4}.wp-block-separator{border:none;border-bottom:2px solid;margin-left:auto;margin-right:auto}.wp-block-separator.has-alpha-channel-opacity{opacity:1}.wp-block-separator:not(.is-style-wide):not(.is-style-dots){width:100px}.wp-block-separator.has-background:not(.is-style-dots){border-bottom:none;height:1px}.wp-block-separator.has-background:not(.is-style-wide):not(.is-style-dots){height:2px}.wp-block-table{margin:"0 0 1em 0"}.wp-block-table thead{border-bottom:3px solid}.wp-block-table tfoot{border-top:3px solid}.wp-block-table td,.wp-block-table th{word-break:normal}.wp-block-table figcaption{color:#555;font-size:13px;text-align:center}.is-dark-theme .wp-block-table figcaption{color:hsla(0,0%,100%,.65)}.wp-block-video figcaption{color:#555;font-size:13px;text-align:center}.is-dark-theme .wp-block-video figcaption{color:hsla(0,0%,100%,.65)}.wp-block-video{margin:0 0 1em}.wp-block-template-part.has-background{padding:1.25em 2.375em;margin-top:0;margin-bottom:0}
</style>
<link rel='stylesheet' id='wp-block-library-css' href='https://dtconsultancy.wpengine.com/wp-includes/css/dist/block-library/style.min.css?ver=6.1.1' type='text/css' media='all' />
<link rel='stylesheet' id='wc-block-vendors-style-css' href='https://dtconsultancy.wpengine.com/wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/vendors-style.css?ver=3.8.1' type='text/css' media='all' />
<link rel='stylesheet' id='wc-block-style-css' href='https://dtconsultancy.wpengine.com/wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/style.css?ver=3.8.1' type='text/css' media='all' />
<link rel='stylesheet' id='jquery-selectBox-css' href='https://dtconsultancy.wpengine.com/wp-content/plugins/yith-woocommerce-wishlist/assets/css/jquery.selectBox.css?ver=1.2.0' type='text/css' media='all' />
<link rel='stylesheet' id='yith-wcwl-font-awesome-css' href='https://dtconsultancy.wpengine.com/wp-content/plugins/yith-woocommerce-wishlist/assets/css/font-awesome.css?ver=4.7.0' type='text/css' media='all' />
<link rel='stylesheet' id='yith-wcwl-main-css' href='https://dtconsultancy.wpengine.com/wp-content/plugins/yith-woocommerce-wishlist/assets/css/style.css?ver=3.0.17' type='text/css' media='all' />
<link rel='stylesheet' id='classic-theme-styles-css' href='https://dtconsultancy.wpengine.com/wp-includes/css/classic-themes.min.css?ver=1' type='text/css' media='all' />
<style id='global-styles-inline-css' type='text/css'>
body{--wp--preset--color--black: #000000;--wp--preset--color--cyan-bluish-gray: #abb8c3;--wp--preset--color--white: #ffffff;--wp--preset--color--pale-pink: #f78da7;--wp--preset--color--vivid-red: #cf2e2e;--wp--preset--color--luminous-vivid-orange: #ff6900;--wp--preset--color--luminous-vivid-amber: #fcb900;--wp--preset--color--light-green-cyan: #7bdcb5;--wp--preset--color--vivid-green-cyan: #00d084;--wp--preset--color--pale-cyan-blue: #8ed1fc;--wp--preset--color--vivid-cyan-blue: #0693e3;--wp--preset--color--vivid-purple: #9b51e0;--wp--preset--color--primary: #274584;--wp--preset--color--secondary: #e3f6f9;--wp--preset--color--tertiary: #7699e3;--wp--preset--color--quaternary: #7699e3;--wp--preset--gradient--vivid-cyan-blue-to-vivid-purple: linear-gradient(135deg,rgba(6,147,227,1) 0%,rgb(155,81,224) 100%);--wp--preset--gradient--light-green-cyan-to-vivid-green-cyan: linear-gradient(135deg,rgb(122,220,180) 0%,rgb(0,208,130) 100%);--wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange: linear-gradient(135deg,rgba(252,185,0,1) 0%,rgba(255,105,0,1) 100%);--wp--preset--gradient--luminous-vivid-orange-to-vivid-red: linear-gradient(135deg,rgba(255,105,0,1) 0%,rgb(207,46,46) 100%);--wp--preset--gradient--very-light-gray-to-cyan-bluish-gray: linear-gradient(135deg,rgb(238,238,238) 0%,rgb(169,184,195) 100%);--wp--preset--gradient--cool-to-warm-spectrum: linear-gradient(135deg,rgb(74,234,220) 0%,rgb(151,120,209) 20%,rgb(207,42,186) 40%,rgb(238,44,130) 60%,rgb(251,105,98) 80%,rgb(254,248,76) 100%);--wp--preset--gradient--blush-light-purple: linear-gradient(135deg,rgb(255,206,236) 0%,rgb(152,150,240) 100%);--wp--preset--gradient--blush-bordeaux: linear-gradient(135deg,rgb(254,205,165) 0%,rgb(254,45,45) 50%,rgb(107,0,62) 100%);--wp--preset--gradient--luminous-dusk: linear-gradient(135deg,rgb(255,203,112) 0%,rgb(199,81,192) 50%,rgb(65,88,208) 100%);--wp--preset--gradient--pale-ocean: linear-gradient(135deg,rgb(255,245,203) 0%,rgb(182,227,212) 50%,rgb(51,167,181) 100%);--wp--preset--gradient--electric-grass: linear-gradient(135deg,rgb(202,248,128) 0%,rgb(113,206,126) 100%);--wp--preset--gradient--midnight: linear-gradient(135deg,rgb(2,3,129) 0%,rgb(40,116,252) 100%);--wp--preset--duotone--dark-grayscale: url('#wp-duotone-dark-grayscale');--wp--preset--duotone--grayscale: url('#wp-duotone-grayscale');--wp--preset--duotone--purple-yellow: url('#wp-duotone-purple-yellow');--wp--preset--duotone--blue-red: url('#wp-duotone-blue-red');--wp--preset--duotone--midnight: url('#wp-duotone-midnight');--wp--preset--duotone--magenta-yellow: url('#wp-duotone-magenta-yellow');--wp--preset--duotone--purple-green: url('#wp-duotone-purple-green');--wp--preset--duotone--blue-orange: url('#wp-duotone-blue-orange');--wp--preset--font-size--small: 13px;--wp--preset--font-size--medium: 20px;--wp--preset--font-size--large: 36px;--wp--preset--font-size--x-large: 42px;--wp--preset--spacing--20: 0.44rem;--wp--preset--spacing--30: 0.67rem;--wp--preset--spacing--40: 1rem;--wp--preset--spacing--50: 1.5rem;--wp--preset--spacing--60: 2.25rem;--wp--preset--spacing--70: 3.38rem;--wp--preset--spacing--80: 5.06rem;}:where(.is-layout-flex){gap: 0.5em;}body .is-layout-flow > .alignleft{float: left;margin-inline-start: 0;margin-inline-end: 2em;}body .is-layout-flow > .alignright{float: right;margin-inline-start: 2em;margin-inline-end: 0;}body .is-layout-flow > .aligncenter{margin-left: auto !important;margin-right: auto !important;}body .is-layout-constrained > .alignleft{float: left;margin-inline-start: 0;margin-inline-end: 2em;}body .is-layout-constrained > .alignright{float: right;margin-inline-start: 2em;margin-inline-end: 0;}body .is-layout-constrained > .aligncenter{margin-left: auto !important;margin-right: auto !important;}body .is-layout-constrained > :where(:not(.alignleft):not(.alignright):not(.alignfull)){max-width: var(--wp--style--global--content-size);margin-left: auto !important;margin-right: auto !important;}body .is-layout-constrained > .alignwide{max-width: var(--wp--style--global--wide-size);}body .is-layout-flex{display: flex;}body .is-layout-flex{flex-wrap: wrap;align-items: center;}body .is-layout-flex > *{margin: 0;}:where(.wp-block-columns.is-layout-flex){gap: 2em;}.has-black-color{color: var(--wp--preset--color--black) !important;}.has-cyan-bluish-gray-color{color: var(--wp--preset--color--cyan-bluish-gray) !important;}.has-white-color{color: var(--wp--preset--color--white) !important;}.has-pale-pink-color{color: var(--wp--preset--color--pale-pink) !important;}.has-vivid-red-color{color: var(--wp--preset--color--vivid-red) !important;}.has-luminous-vivid-orange-color{color: var(--wp--preset--color--luminous-vivid-orange) !important;}.has-luminous-vivid-amber-color{color: var(--wp--preset--color--luminous-vivid-amber) !important;}.has-light-green-cyan-color{color: var(--wp--preset--color--light-green-cyan) !important;}.has-vivid-green-cyan-color{color: var(--wp--preset--color--vivid-green-cyan) !important;}.has-pale-cyan-blue-color{color: var(--wp--preset--color--pale-cyan-blue) !important;}.has-vivid-cyan-blue-color{color: var(--wp--preset--color--vivid-cyan-blue) !important;}.has-vivid-purple-color{color: var(--wp--preset--color--vivid-purple) !important;}.has-black-background-color{background-color: var(--wp--preset--color--black) !important;}.has-cyan-bluish-gray-background-color{background-color: var(--wp--preset--color--cyan-bluish-gray) !important;}.has-white-background-color{background-color: var(--wp--preset--color--white) !important;}.has-pale-pink-background-color{background-color: var(--wp--preset--color--pale-pink) !important;}.has-vivid-red-background-color{background-color: var(--wp--preset--color--vivid-red) !important;}.has-luminous-vivid-orange-background-color{background-color: var(--wp--preset--color--luminous-vivid-orange) !important;}.has-luminous-vivid-amber-background-color{background-color: var(--wp--preset--color--luminous-vivid-amber) !important;}.has-light-green-cyan-background-color{background-color: var(--wp--preset--color--light-green-cyan) !important;}.has-vivid-green-cyan-background-color{background-color: var(--wp--preset--color--vivid-green-cyan) !important;}.has-pale-cyan-blue-background-color{background-color: var(--wp--preset--color--pale-cyan-blue) !important;}.has-vivid-cyan-blue-background-color{background-color: var(--wp--preset--color--vivid-cyan-blue) !important;}.has-vivid-purple-background-color{background-color: var(--wp--preset--color--vivid-purple) !important;}.has-black-border-color{border-color: var(--wp--preset--color--black) !important;}.has-cyan-bluish-gray-border-color{border-color: var(--wp--preset--color--cyan-bluish-gray) !important;}.has-white-border-color{border-color: var(--wp--preset--color--white) !important;}.has-pale-pink-border-color{border-color: var(--wp--preset--color--pale-pink) !important;}.has-vivid-red-border-color{border-color: var(--wp--preset--color--vivid-red) !important;}.has-luminous-vivid-orange-border-color{border-color: var(--wp--preset--color--luminous-vivid-orange) !important;}.has-luminous-vivid-amber-border-color{border-color: var(--wp--preset--color--luminous-vivid-amber) !important;}.has-light-green-cyan-border-color{border-color: var(--wp--preset--color--light-green-cyan) !important;}.has-vivid-green-cyan-border-color{border-color: var(--wp--preset--color--vivid-green-cyan) !important;}.has-pale-cyan-blue-border-color{border-color: var(--wp--preset--color--pale-cyan-blue) !important;}.has-vivid-cyan-blue-border-color{border-color: var(--wp--preset--color--vivid-cyan-blue) !important;}.has-vivid-purple-border-color{border-color: var(--wp--preset--color--vivid-purple) !important;}.has-vivid-cyan-blue-to-vivid-purple-gradient-background{background: var(--wp--preset--gradient--vivid-cyan-blue-to-vivid-purple) !important;}.has-light-green-cyan-to-vivid-green-cyan-gradient-background{background: var(--wp--preset--gradient--light-green-cyan-to-vivid-green-cyan) !important;}.has-luminous-vivid-amber-to-luminous-vivid-orange-gradient-background{background: var(--wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange) !important;}.has-luminous-vivid-orange-to-vivid-red-gradient-background{background: var(--wp--preset--gradient--luminous-vivid-orange-to-vivid-red) !important;}.has-very-light-gray-to-cyan-bluish-gray-gradient-background{background: var(--wp--preset--gradient--very-light-gray-to-cyan-bluish-gray) !important;}.has-cool-to-warm-spectrum-gradient-background{background: var(--wp--preset--gradient--cool-to-warm-spectrum) !important;}.has-blush-light-purple-gradient-background{background: var(--wp--preset--gradient--blush-light-purple) !important;}.has-blush-bordeaux-gradient-background{background: var(--wp--preset--gradient--blush-bordeaux) !important;}.has-luminous-dusk-gradient-background{background: var(--wp--preset--gradient--luminous-dusk) !important;}.has-pale-ocean-gradient-background{background: var(--wp--preset--gradient--pale-ocean) !important;}.has-electric-grass-gradient-background{background: var(--wp--preset--gradient--electric-grass) !important;}.has-midnight-gradient-background{background: var(--wp--preset--gradient--midnight) !important;}.has-small-font-size{font-size: var(--wp--preset--font-size--small) !important;}.has-medium-font-size{font-size: var(--wp--preset--font-size--medium) !important;}.has-large-font-size{font-size: var(--wp--preset--font-size--large) !important;}.has-x-large-font-size{font-size: var(--wp--preset--font-size--x-large) !important;}
.wp-block-navigation a:where(:not(.wp-element-button)){color: inherit;}
:where(.wp-block-columns.is-layout-flex){gap: 2em;}
.wp-block-pullquote{font-size: 1.5em;line-height: 1.6;}
</style>
<link rel='stylesheet' id='contact-form-7-css' href='css/styles.css?ver=5.3.2' type='text/css' media='all' />
<link rel='stylesheet' id='dt-animation-css-css' href='css/animations.css?ver=6.1.1' type='text/css' media='all' />
<link rel='stylesheet' id='dt-slick-css-css' href='css/slick.css?ver=6.1.1' type='text/css' media='all' />
<link rel='stylesheet' id='dt-sc-css-css' href='https://dtconsultancy.wpengine.com/wp-content/plugins/designthemes-core-features/shortcodes/css/shortcodes.css?ver=6.1.1' type='text/css' media='all' />
<link rel='stylesheet' id='rs-plugin-settings-css' href='css/rs6.css?ver=6.3.3' type='text/css' media='all' />
<style id='rs-plugin-settings-inline-css' type='text/css'>
#rs-demo-id {}
</style>
<style id='woocommerce-inline-inline-css' type='text/css'>
.woocommerce form .form-row .required { visibility: visible; }
</style>
<link rel='stylesheet' id='wpsl-styles-css' href='css/styles.min.css?ver=2.2.233' type='text/css' media='all' />
<link rel='stylesheet' id='woocommerce_prettyPhoto_css-css' href='css/prettyPhoto.css?ver=6.1.1' type='text/css' media='all' />
<link rel='stylesheet' id='js_composer_front-css' href='css/js_composer.min.css?ver=6.5.0' type='text/css' media='all' />
<link rel='stylesheet' id='bsf-Defaults-css' href='css/Defaults.css?ver=3.19.8' type='text/css' media='all' />
<link rel='stylesheet' id='ultimate-style-css' href='css/style.min.css?ver=3.19.8' type='text/css' media='all' />
<link rel='stylesheet' id='ultimate-animate-css' href='css/animate.min.css?ver=3.19.8' type='text/css' media='all' />
<link rel='stylesheet' id='ult-slick-css' href='css/slick.min.css?ver=3.19.8' type='text/css' media='all' />
<link rel='stylesheet' id='ult-icons-css' href='css/icons.css?ver=3.19.8' type='text/css' media='all' />
<link rel='stylesheet' id='consultancy-css' href='css/style.css?ver=2.3' type='text/css' media='all' />


<link rel='stylesheet' id='consultancy-base-css' href='css/base.css?ver=2.3' type='text/css' media='all' />
<link rel='stylesheet' id='consultancy-grid-css' href='css/grid.css?ver=2.3' type='text/css' media='all' />
<link rel='stylesheet' id='consultancy-widget-css' href='css/widget.css?ver=2.3' type='text/css' media='all' />
<link rel='stylesheet' id='consultancy-layout-css' href='css/layout.css?ver=2.3' type='text/css' media='all' />
<link rel='stylesheet' id='consultancy-blog-css' href='css/blog.css?ver=2.3' type='text/css' media='all' />
<link rel='stylesheet' id='consultancy-portfolio-css' href='css/portfolio.css?ver=2.3' type='text/css' media='all' />
<link rel='stylesheet' id='consultancy-contact-css' href='css/contact.css?ver=2.3' type='text/css' media='all' />
<link rel='stylesheet' id='consultancy-custom-class-css' href='css/custom-class.css?ver=2.3' type='text/css' media='all' />
<link rel='stylesheet' id='prettyphoto-css' href='css/prettyPhoto.min.css?ver=6.5.0' type='text/css' media='all' />
<link rel='stylesheet' id='custom-font-awesome-css' href='css/font-awesome.min.css?ver=4.3.0' type='text/css' media='all' />

<link rel='stylesheet' id='pe-icon-7-stroke-css' href='css/pe-icon-7-stroke.css?ver=6.1.1' type='text/css' media='all' />
<link rel='stylesheet' id='stroke-gap-icons-style-css' href='css/stroke-gap-icons-style.css?ver=6.1.1' type='text/css' media='all' />
<link rel='stylesheet' id='icon-moon-css' href='css/icon-moon.css?ver=6.1.1' type='text/css' media='all' />
<link rel='stylesheet' id='material-design-iconic-css' href='css/material-design-iconic-font.min.css?ver=6.1.1' type='text/css' media='all' />
<link rel='stylesheet' id='consultancy-loader-css' href='css/loaders.css?ver=2.3' type='text/css' media='all' />
<link rel='stylesheet' id='consultancy-woo-css' href='css/woocommerce.css?ver=2.3' type='text/css' media='all' />



<link rel='stylesheet' id='consultancy-skin-css' href='css/styles1.css?ver=6.1.1' type='text/css' media='all' />
<link rel='stylesheet' id='consultancy-customevent-css' href='css/custom_1.css?ver=2.3' type='text/css' media='all' />
<link rel='stylesheet' id='consultancy-popup-css-css' href='css/magnific-popup.css?ver=2.3' type='text/css' media='all' />
<link rel='stylesheet' id='consultancy-gutenberg-css' href='css/gutenberg.css?ver=2.3' type='text/css' media='all' />


<script type='text/javascript' src='js/jquery.min.js?ver=3.6.1' id='jquery-core-js'></script>
<script type='text/javascript' src='js/jquery-migrate.min.js?ver=3.3.2' id='jquery-migrate-js'></script>
<script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/plugins/revslider/public/assets/js/rbtools.min.js?ver=6.3.3' id='tp-tools-js'></script>
<script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/plugins/revslider/public/assets/js/rs6.min.js?ver=6.3.3' id='revmin-js'></script>
<script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js?ver=2.70' id='jquery-blockui-js'></script>

<script type='text/javascript' src='js/add-to-cart.min.js?ver=4.8.3' id='wc-add-to-cart-js'></script>
<script type='text/javascript' src='js/woocommerce-add-to-cart.js?ver=6.5.0' id='vc_woocommerce-add-to-cart-js-js'></script>
<script type='text/javascript' src='js/ultimate-params.min.js?ver=3.19.8' id='ultimate-vc-params-js'></script>
<script type='text/javascript' src='js/custom.min.js?ver=3.19.8' id='ultimate-custom-js'></script>
<script type='text/javascript' src='js/jquery-appear.min.js?ver=3.19.8' id='ultimate-appear-js'></script>
<script type='text/javascript' src='js/slick.min.js?ver=3.19.8' id='ult-slick-js'></script>
<script type='text/javascript' src='js/slick-custom.min.js?ver=3.19.8' id='ult-slick-custom-js'></script>
<script type='text/javascript' src='js/modernizr.custom.js?ver=6.1.1' id='modernizr-custom-js'></script>


<link rel="https://api.w.org/" href="https://dtconsultancy.wpengine.com/wp-json/" /><link rel="alternate" type="application/json" href="https://dtconsultancy.wpengine.com/wp-json/wp/v2/pages/7467" /><link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://dtconsultancy.wpengine.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://dtconsultancy.wpengine.com/wp-includes/wlwmanifest.xml" />
<link rel="canonical" href="https://dtconsultancy.wpengine.com/" />
<link rel='shortlink' href='https://dtconsultancy.wpengine.com/' />
<link rel="alternate" type="application/json+oembed" href="https://dtconsultancy.wpengine.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fdtconsultancy.wpengine.com%2F" />
<link rel="alternate" type="text/xml+oembed" href="https://dtconsultancy.wpengine.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fdtconsultancy.wpengine.com%2F&#038;format=xml" />
	<noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>
	<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style><meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress."/>
<meta name="generator" content="Powered by Slider Revolution 6.3.3 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
<link rel="icon" href=  "templates/R2.png" sizes="32x32" />
<link rel="icon" href="templates/R2.png" sizes="192x192" />
<link rel="apple-touch-icon" href="https://dtconsultancy.wpengine.com/wp-content/uploads/2017/06/cropped-apple-touch-icon-512x512-180x180.png" />
<meta name="msapplication-TileImage" content="https://dtconsultancy.wpengine.com/wp-content/uploads/2017/06/cropped-apple-touch-icon-512x512-270x270.png" />
<script type="text/javascript">function setREVStartSize(e){
			//window.requestAnimationFrame(function() {				 
				window.RSIW = window.RSIW===undefined ? window.innerWidth : window.RSIW;	
				window.RSIH = window.RSIH===undefined ? window.innerHeight : window.RSIH;	
				try {								
					var pw = document.getElementById(e.c).parentNode.offsetWidth,
						newh;
					pw = pw===0 || isNaN(pw) ? window.RSIW : pw;
					e.tabw = e.tabw===undefined ? 0 : parseInt(e.tabw);
					e.thumbw = e.thumbw===undefined ? 0 : parseInt(e.thumbw);
					e.tabh = e.tabh===undefined ? 0 : parseInt(e.tabh);
					e.thumbh = e.thumbh===undefined ? 0 : parseInt(e.thumbh);
					e.tabhide = e.tabhide===undefined ? 0 : parseInt(e.tabhide);
					e.thumbhide = e.thumbhide===undefined ? 0 : parseInt(e.thumbhide);
					e.mh = e.mh===undefined || e.mh=="" || e.mh==="auto" ? 0 : parseInt(e.mh,0);		
					if(e.layout==="fullscreen" || e.l==="fullscreen") 						
						newh = Math.max(e.mh,window.RSIH);					
					else{					
						e.gw = Array.isArray(e.gw) ? e.gw : [e.gw];
						for (var i in e.rl) if (e.gw[i]===undefined || e.gw[i]===0) e.gw[i] = e.gw[i-1];					
						e.gh = e.el===undefined || e.el==="" || (Array.isArray(e.el) && e.el.length==0)? e.gh : e.el;
						e.gh = Array.isArray(e.gh) ? e.gh : [e.gh];
						for (var i in e.rl) if (e.gh[i]===undefined || e.gh[i]===0) e.gh[i] = e.gh[i-1];
											
						var nl = new Array(e.rl.length),
							ix = 0,						
							sl;					
						e.tabw = e.tabhide>=pw ? 0 : e.tabw;
						e.thumbw = e.thumbhide>=pw ? 0 : e.thumbw;
						e.tabh = e.tabhide>=pw ? 0 : e.tabh;
						e.thumbh = e.thumbhide>=pw ? 0 : e.thumbh;					
						for (var i in e.rl) nl[i] = e.rl[i]<window.RSIW ? 0 : e.rl[i];
						sl = nl[0];									
						for (var i in nl) if (sl>nl[i] && nl[i]>0) { sl = nl[i]; ix=i;}															
						var m = pw>(e.gw[ix]+e.tabw+e.thumbw) ? 1 : (pw-(e.tabw+e.thumbw)) / (e.gw[ix]);					
						newh =  (e.gh[ix] * m) + (e.tabh + e.thumbh);
					}				
					if(window.rs_init_css===undefined) window.rs_init_css = document.head.appendChild(document.createElement("style"));					
					document.getElementById(e.c).height = newh+"px";
					window.rs_init_css.innerHTML += "#"+e.c+"_wrapper { height: "+newh+"px }";				
				} catch(e){
					console.log("Failure at Presize of Slider:" + e)
				}					   
			//});
		  };</script>
<style id="kirki-inline-styles">body, .layout-boxed .inner-wrapper, .secondary-sidebar .type8 .widgettitle, .secondary-sidebar .type10 .widgettitle:after, .dt-sc-contact-info.type3::after, .dt-sc-image-caption .dt-sc-image-wrapper .icon-wrapper::after, ul.products li .product-wrapper, .woocommerce-tabs .panel, .select2-results, .woocommerce .woocommerce-message, .woocommerce .woocommerce-info, .woocommerce .woocommerce-error, .woocommerce div.product .woocommerce-tabs ul.tabs li.active, .woo-type13 ul.products li.product:hover .product-details h5 a, .tribe-events-list-separator-month span{background-color:#ffffff;}.dt-sc-image-caption.type8 .dt-sc-image-content::before{border-color:#ffffff;}.secondary-sidebar .type14 .widgettitle:before, .widget.buddypress div.item-options a.selected{border-bottom-color:#ffffff;}.dt-sc-testimonial.type2 blockquote::before{border-top-color:#ffffff;}body, .wp-block-pullquote{color:#4d4d4d;}a{color:#274584;}a:hover{color:#888888;}.main-title-section h1, h1.simple-title{font-family:Raleway;font-size:24px;font-weight:600;text-transform:none;color:#ffffff;}div.breadcrumb a{font-family:Poppins;font-weight:400;text-transform:none;color:#333333;}div.footer-widgets{background-image:url("https://dtconsultancy.wpengine.com/wp-content/uploads/2017/06/footer-bg.jpg");background-position:center center;background-repeat:no-repeat;}div.footer-widgets h3.widgettitle{font-family:Raleway;font-size:20px;font-weight:700;line-height:36px;text-align:left;text-transform:none;color:#2b2b2b;}div.footer-widgets .widget{font-family:Poppins;font-size:14px;font-weight:400;line-height:28px;text-align:left;text-transform:none;color:#333333;}#main-menu > ul.menu > li > a, #main-menu > ul.menu > li > .nolink-menu{font-family:Poppins;font-weight:400;text-transform:none;color:#2f2f2f;}body{font-family:Poppins;font-size:14px;font-weight:400;letter-spacing:0.5px;line-height:28px;text-transform:none;color:#4d4d4d;}input[type="text"], input[type="password"], input[type="email"], input[type="url"], input[type="tel"], input[type="number"], input[type="range"], input[type="date"], textarea, input.text, input[type="search"], select, textarea, #main-menu ul.menu > li > a, .dt-sc-counter.type1 .dt-sc-counter-number, .dt-sc-portfolio-sorting a, .dt-sc-testimonial.type1 blockquote, .entry-meta, .dt-sc-testimonial .dt-sc-testimonial-author cite, .dt-sc-pr-tb-col.minimal .dt-sc-price p, .dt-sc-pr-tb-col.minimal .dt-sc-price h6 span, .dt-sc-testimonial.special-testimonial-carousel blockquote, .dt-sc-pr-tb-col .dt-sc-tb-title, .dt-sc-pr-tb-col .dt-sc-tb-content, .dt-sc-button, .dt-sc-bar-text, input[type="submit"], input[type="reset"], blockquote.type1, .dt-sc-testimonial.type5 .dt-sc-testimonial-quote blockquote, .dt-sc-testimonial.type5 .dt-sc-testimonial-author cite:before, .dt-sc-testimonial.type1 q:before, .dt-sc-testimonial.type1 q:after, .dt-sc-title.script-with-sub-title h4, .dt-sc-title.script-with-sub-title h5, .dt-sc-title.script-with-sub-title h6, .type2.heading-with-button h5.dt-sc-toggle-accordion:before, .type2.heading-with-button h5.dt-sc-toggle:before, .dt-sc-toggle-frame-set .dt-sc-toggle-accordion a strong, h5.dt-sc-toggle a strong, .dt-sc-image-caption.type5 p, .custom-request-form .ipt-uif-custom-material-custom .input-field label, .custom-request-form .ipt-uif-custom-material-custom .ui-button .ui-button-text, .custom-request-form .ipt-uif-custom-material-custom .ipt_uif_message .ui-widget-content p, .custom-request-form .ipt-uif-custom-material-custom .ipt_fsqm_form_message_success .ui-widget-content p, .custom-request-form .ipt-uif-custom-material-custom .ipt_fsqm_form_message_error .ui-widget-content p, .custom-request-form .ipt-uif-custom-material-custom .ipt_fsqm_form_validation_error .ui-widget-content p, .custom-request-form .ipt-uif-custom-material-custom .ipt_fsqm_form_message_restore .ui-widget-content p, .custom-request-form .ipt-uif-custom-material-custom .eform-styled-widget .ui-widget-content p, .custom-request-form .ipt-uif-custom-material-custom a{font-family:Poppins;}.dt-sc-tabs-horizontal-frame-container.type3 ul.dt-sc-tabs-horizontal-frame > li > a, .dt-sc-icon-box.type2 .icon-content h4, .dt-sc-team.simple-rounded .dt-sc-team-details h4, .type2 h5.dt-sc-toggle-accordion, .type2 h5.dt-sc-toggle, .dt-sc-counter.type2 .dt-sc-counter-number, .dt-sc-icon-box.type8 .icon-content h4, h5.dt-sc-donutchart-title, .donutchart-text, .dt-sc-progress-wrapper .dt-sc-bar-title, .dt-sc-team.without-bg .dt-sc-team-details h4, .dt-sc-team.without-bg .dt-sc-team-details h5, .dt-sc-icon-box.type5.bordered .icon-content h4, h5.dt-sc-toggle-accordion, h5.dt-sc-toggle, ul.dt-sc-tabs-horizontal > li > a, ul.dt-sc-tabs-vertical > li > a, ul.dt-sc-tabs-horizontal-frame > li > a, ul.dt-sc-tabs-horizontal-frame, ul.dt-sc-tabs-vertical-frame > li > a, .dt-sc-image-caption.type5 h3, .dt-sc-image-caption.type5 h6, .dt-sc-counter.type1.decorated .dt-sc-counter-number, .dt-sc-icon-box.type1 .icon-content h4, .dt-sc-newsletter-section.type7 .dt-sc-subscribe-frm input[type="submit"], .dt-sc-map-form-holder, .dt-sc-pr-tb-col, .dt-sc-pr-tb-col .dt-sc-price h6, .dt-sc-team h1, .dt-sc-team h2, .dt-sc-team h3, .dt-sc-team h4, .dt-sc-team h5, .dt-sc-team h6, .dt-sc-colored-big-buttons, .custom-request-form .ipt-uif-custom-material-custom *, .custom-request-form .ipt-uif-custom-material-custom select, .custom-request-form .ipt-uif-custom-material-custom option, .dt-sc-testimonial-wrapper .dt-sc-testimonial.type1.masonry .dt-sc-testimonial-author cite, #toTop, ul.side-nav li a, .under-construction.type2 p, .under-construction.type2 .downcount .dt-sc-counter-wrapper:first-child h3, body div.pp_default .pp_description, .blog-entry.single .entry-title h4, .post-nav-container .post-next-link a, .post-nav-container .post-prev-link a{font-family:Raleway;}h1{font-family:Raleway;font-weight:700;text-align:inherit;text-transform:none;color:#4d4d4d;}h2{font-family:Raleway;font-weight:600;text-align:inherit;text-transform:none;color:#4d4d4d;}h3{font-family:Raleway;font-weight:600;text-align:inherit;text-transform:none;color:#4d4d4d;}h4{font-family:Poppins;font-weight:400;text-align:inherit;text-transform:none;color:#4d4d4d;}h5{font-family:Poppins;font-weight:300;text-align:inherit;text-transform:none;color:#4d4d4d;}h6{font-family:Poppins;font-weight:400;text-align:inherit;text-transform:none;color:#4d4d4d;}/* cyrillic-ext */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 100;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4WjNDr4fJh1Zyc6FYxlG.woff) format('woff');
  unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
}
/* cyrillic */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 100;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4WjNDrcfJh1Zyc6FYxlG.woff) format('woff');
  unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
}
/* vietnamese */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 100;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4WjNDrwfJh1Zyc6FYxlG.woff) format('woff');
  unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
}
/* latin-ext */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 100;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4WjNDr0fJh1Zyc6FYxlG.woff) format('woff');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 100;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4WjNDrMfJh1Zyc6FYw.woff) format('woff');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* cyrillic-ext */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 200;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4ejMDr4fJh1Zyc6FYxlG.woff) format('woff');
  unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
}
/* cyrillic */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 200;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4ejMDrcfJh1Zyc6FYxlG.woff) format('woff');
  unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
}
/* vietnamese */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 200;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4ejMDrwfJh1Zyc6FYxlG.woff) format('woff');
  unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
}
/* latin-ext */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 200;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4ejMDr0fJh1Zyc6FYxlG.woff) format('woff');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 200;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4ejMDrMfJh1Zyc6FYw.woff) format('woff');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* cyrillic-ext */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 300;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4TbMDr4fJh1Zyc6FYxlG.woff) format('woff');
  unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
}
/* cyrillic */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 300;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4TbMDrcfJh1Zyc6FYxlG.woff) format('woff');
  unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
}
/* vietnamese */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 300;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4TbMDrwfJh1Zyc6FYxlG.woff) format('woff');
  unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
}
/* latin-ext */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 300;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4TbMDr0fJh1Zyc6FYxlG.woff) format('woff');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 300;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4TbMDrMfJh1Zyc6FYw.woff) format('woff');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* cyrillic-ext */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 400;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4WjMDr4fJh1Zyc6FYxlG.woff) format('woff');
  unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
}
/* cyrillic */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 400;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4WjMDrcfJh1Zyc6FYxlG.woff) format('woff');
  unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
}
/* vietnamese */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 400;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4WjMDrwfJh1Zyc6FYxlG.woff) format('woff');
  unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
}
/* latin-ext */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 400;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4WjMDr0fJh1Zyc6FYxlG.woff) format('woff');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 400;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4WjMDrMfJh1Zyc6FYw.woff) format('woff');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* cyrillic-ext */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 500;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4VrMDr4fJh1Zyc6FYxlG.woff) format('woff');
  unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
}
/* cyrillic */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 500;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4VrMDrcfJh1Zyc6FYxlG.woff) format('woff');
  unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
}
/* vietnamese */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 500;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4VrMDrwfJh1Zyc6FYxlG.woff) format('woff');
  unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
}
/* latin-ext */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 500;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4VrMDr0fJh1Zyc6FYxlG.woff) format('woff');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 500;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4VrMDrMfJh1Zyc6FYw.woff) format('woff');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* cyrillic-ext */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 600;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4bbLDr4fJh1Zyc6FYxlG.woff) format('woff');
  unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
}
/* cyrillic */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 600;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4bbLDrcfJh1Zyc6FYxlG.woff) format('woff');
  unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
}
/* vietnamese */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 600;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4bbLDrwfJh1Zyc6FYxlG.woff) format('woff');
  unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
}
/* latin-ext */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 600;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4bbLDr0fJh1Zyc6FYxlG.woff) format('woff');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 600;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4bbLDrMfJh1Zyc6FYw.woff) format('woff');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* cyrillic-ext */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 700;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4Y_LDr4fJh1Zyc6FYxlG.woff) format('woff');
  unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
}
/* cyrillic */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 700;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4Y_LDrcfJh1Zyc6FYxlG.woff) format('woff');
  unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
}
/* vietnamese */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 700;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4Y_LDrwfJh1Zyc6FYxlG.woff) format('woff');
  unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
}
/* latin-ext */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 700;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4Y_LDr0fJh1Zyc6FYxlG.woff) format('woff');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 700;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4Y_LDrMfJh1Zyc6FYw.woff) format('woff');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* cyrillic-ext */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 800;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4ejLDr4fJh1Zyc6FYxlG.woff) format('woff');
  unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
}
/* cyrillic */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 800;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4ejLDrcfJh1Zyc6FYxlG.woff) format('woff');
  unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
}
/* vietnamese */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 800;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4ejLDrwfJh1Zyc6FYxlG.woff) format('woff');
  unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
}
/* latin-ext */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 800;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4ejLDr0fJh1Zyc6FYxlG.woff) format('woff');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 800;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4ejLDrMfJh1Zyc6FYw.woff) format('woff');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* cyrillic-ext */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 900;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4cHLDr4fJh1Zyc6FYxlG.woff) format('woff');
  unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
}
/* cyrillic */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 900;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4cHLDrcfJh1Zyc6FYxlG.woff) format('woff');
  unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
}
/* vietnamese */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 900;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4cHLDrwfJh1Zyc6FYxlG.woff) format('woff');
  unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
}
/* latin-ext */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 900;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4cHLDr0fJh1Zyc6FYxlG.woff) format('woff');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Raleway';
  font-style: italic;
  font-weight: 900;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Pt_g8zYS_SKggPNyCgSQamb1W0lwk4S4cHLDrMfJh1Zyc6FYw.woff) format('woff');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* cyrillic-ext */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 100;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVvao7CFPrcVIT9d4cydYA.woff) format('woff');
  unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
}
/* cyrillic */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 100;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVvao7CMPrcVIT9d4cydYA.woff) format('woff');
  unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
}
/* vietnamese */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 100;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVvao7CHPrcVIT9d4cydYA.woff) format('woff');
  unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
}
/* latin-ext */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 100;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVvao7CGPrcVIT9d4cydYA.woff) format('woff');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 100;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVvao7CIPrcVIT9d4cw.woff) format('woff');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* cyrillic-ext */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 200;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVtaorCFPrcVIT9d4cydYA.woff) format('woff');
  unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
}
/* cyrillic */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 200;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVtaorCMPrcVIT9d4cydYA.woff) format('woff');
  unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
}
/* vietnamese */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 200;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVtaorCHPrcVIT9d4cydYA.woff) format('woff');
  unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
}
/* latin-ext */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 200;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVtaorCGPrcVIT9d4cydYA.woff) format('woff');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 200;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVtaorCIPrcVIT9d4cw.woff) format('woff');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* cyrillic-ext */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 300;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVuEorCFPrcVIT9d4cydYA.woff) format('woff');
  unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
}
/* cyrillic */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 300;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVuEorCMPrcVIT9d4cydYA.woff) format('woff');
  unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
}
/* vietnamese */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 300;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVuEorCHPrcVIT9d4cydYA.woff) format('woff');
  unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
}
/* latin-ext */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 300;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVuEorCGPrcVIT9d4cydYA.woff) format('woff');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 300;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVuEorCIPrcVIT9d4cw.woff) format('woff');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* cyrillic-ext */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 400;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVvaorCFPrcVIT9d4cydYA.woff) format('woff');
  unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
}
/* cyrillic */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 400;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVvaorCMPrcVIT9d4cydYA.woff) format('woff');
  unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
}
/* vietnamese */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 400;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVvaorCHPrcVIT9d4cydYA.woff) format('woff');
  unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
}
/* latin-ext */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 400;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVvaorCGPrcVIT9d4cydYA.woff) format('woff');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 400;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVvaorCIPrcVIT9d4cw.woff) format('woff');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* cyrillic-ext */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 500;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVvoorCFPrcVIT9d4cydYA.woff) format('woff');
  unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
}
/* cyrillic */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 500;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVvoorCMPrcVIT9d4cydYA.woff) format('woff');
  unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
}
/* vietnamese */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 500;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVvoorCHPrcVIT9d4cydYA.woff) format('woff');
  unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
}
/* latin-ext */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 500;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVvoorCGPrcVIT9d4cydYA.woff) format('woff');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 500;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVvoorCIPrcVIT9d4cw.woff) format('woff');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* cyrillic-ext */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 600;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVsEpbCFPrcVIT9d4cydYA.woff) format('woff');
  unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
}
/* cyrillic */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 600;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVsEpbCMPrcVIT9d4cydYA.woff) format('woff');
  unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
}
/* vietnamese */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 600;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVsEpbCHPrcVIT9d4cydYA.woff) format('woff');
  unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
}
/* latin-ext */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 600;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVsEpbCGPrcVIT9d4cydYA.woff) format('woff');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 600;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVsEpbCIPrcVIT9d4cw.woff) format('woff');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* cyrillic-ext */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 700;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVs9pbCFPrcVIT9d4cydYA.woff) format('woff');
  unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
}
/* cyrillic */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 700;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVs9pbCMPrcVIT9d4cydYA.woff) format('woff');
  unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
}
/* vietnamese */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 700;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVs9pbCHPrcVIT9d4cydYA.woff) format('woff');
  unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
}
/* latin-ext */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 700;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVs9pbCGPrcVIT9d4cydYA.woff) format('woff');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 700;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVs9pbCIPrcVIT9d4cw.woff) format('woff');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* cyrillic-ext */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 800;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVtapbCFPrcVIT9d4cydYA.woff) format('woff');
  unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
}
/* cyrillic */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 800;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVtapbCMPrcVIT9d4cydYA.woff) format('woff');
  unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
}
/* vietnamese */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 800;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVtapbCHPrcVIT9d4cydYA.woff) format('woff');
  unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
}
/* latin-ext */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 800;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVtapbCGPrcVIT9d4cydYA.woff) format('woff');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 800;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVtapbCIPrcVIT9d4cw.woff) format('woff');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* cyrillic-ext */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 900;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVtzpbCFPrcVIT9d4cydYA.woff) format('woff');
  unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
}
/* cyrillic */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 900;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVtzpbCMPrcVIT9d4cydYA.woff) format('woff');
  unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
}
/* vietnamese */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 900;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVtzpbCHPrcVIT9d4cydYA.woff) format('woff');
  unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
}
/* latin-ext */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 900;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVtzpbCGPrcVIT9d4cydYA.woff) format('woff');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Raleway';
  font-style: normal;
  font-weight: 900;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/raleway/1Ptxg8zYS_SKggPN4iEgvnHyvveLxVtzpbCIPrcVIT9d4cw.woff) format('woff');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}/* devanagari */
@font-face {
  font-family: 'Poppins';
  font-style: italic;
  font-weight: 100;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiAyp8kv8JHgFVrJJLmE0tDMPShSkFEkm8.woff) format('woff');
  unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
}
/* latin-ext */
@font-face {
  font-family: 'Poppins';
  font-style: italic;
  font-weight: 100;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiAyp8kv8JHgFVrJJLmE0tMMPShSkFEkm8.woff) format('woff');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Poppins';
  font-style: italic;
  font-weight: 100;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiAyp8kv8JHgFVrJJLmE0tCMPShSkFE.woff) format('woff');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* devanagari */
@font-face {
  font-family: 'Poppins';
  font-style: italic;
  font-weight: 200;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLmv1pVFteIYktMqlap.woff) format('woff');
  unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
}
/* latin-ext */
@font-face {
  font-family: 'Poppins';
  font-style: italic;
  font-weight: 200;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLmv1pVGdeIYktMqlap.woff) format('woff');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Poppins';
  font-style: italic;
  font-weight: 200;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLmv1pVF9eIYktMqg.woff) format('woff');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* devanagari */
@font-face {
  font-family: 'Poppins';
  font-style: italic;
  font-weight: 300;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLm21lVFteIYktMqlap.woff) format('woff');
  unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
}
/* latin-ext */
@font-face {
  font-family: 'Poppins';
  font-style: italic;
  font-weight: 300;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLm21lVGdeIYktMqlap.woff) format('woff');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Poppins';
  font-style: italic;
  font-weight: 300;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLm21lVF9eIYktMqg.woff) format('woff');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* devanagari */
@font-face {
  font-family: 'Poppins';
  font-style: italic;
  font-weight: 400;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiGyp8kv8JHgFVrJJLucXtGOvWDSHFF.woff) format('woff');
  unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
}
/* latin-ext */
@font-face {
  font-family: 'Poppins';
  font-style: italic;
  font-weight: 400;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiGyp8kv8JHgFVrJJLufntGOvWDSHFF.woff) format('woff');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Poppins';
  font-style: italic;
  font-weight: 400;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiGyp8kv8JHgFVrJJLucHtGOvWDSA.woff) format('woff');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* devanagari */
@font-face {
  font-family: 'Poppins';
  font-style: italic;
  font-weight: 500;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLmg1hVFteIYktMqlap.woff) format('woff');
  unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
}
/* latin-ext */
@font-face {
  font-family: 'Poppins';
  font-style: italic;
  font-weight: 500;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLmg1hVGdeIYktMqlap.woff) format('woff');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Poppins';
  font-style: italic;
  font-weight: 500;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLmg1hVF9eIYktMqg.woff) format('woff');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* devanagari */
@font-face {
  font-family: 'Poppins';
  font-style: italic;
  font-weight: 600;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLmr19VFteIYktMqlap.woff) format('woff');
  unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
}
/* latin-ext */
@font-face {
  font-family: 'Poppins';
  font-style: italic;
  font-weight: 600;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLmr19VGdeIYktMqlap.woff) format('woff');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Poppins';
  font-style: italic;
  font-weight: 600;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLmr19VF9eIYktMqg.woff) format('woff');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* devanagari */
@font-face {
  font-family: 'Poppins';
  font-style: italic;
  font-weight: 700;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLmy15VFteIYktMqlap.woff) format('woff');
  unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
}
/* latin-ext */
@font-face {
  font-family: 'Poppins';
  font-style: italic;
  font-weight: 700;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLmy15VGdeIYktMqlap.woff) format('woff');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Poppins';
  font-style: italic;
  font-weight: 700;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLmy15VF9eIYktMqg.woff) format('woff');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* devanagari */
@font-face {
  font-family: 'Poppins';
  font-style: italic;
  font-weight: 800;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLm111VFteIYktMqlap.woff) format('woff');
  unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
}
/* latin-ext */
@font-face {
  font-family: 'Poppins';
  font-style: italic;
  font-weight: 800;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLm111VGdeIYktMqlap.woff) format('woff');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Poppins';
  font-style: italic;
  font-weight: 800;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLm111VF9eIYktMqg.woff) format('woff');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* devanagari */
@font-face {
  font-family: 'Poppins';
  font-style: italic;
  font-weight: 900;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLm81xVFteIYktMqlap.woff) format('woff');
  unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
}
/* latin-ext */
@font-face {
  font-family: 'Poppins';
  font-style: italic;
  font-weight: 900;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLm81xVGdeIYktMqlap.woff) format('woff');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Poppins';
  font-style: italic;
  font-weight: 900;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiDyp8kv8JHgFVrJJLm81xVF9eIYktMqg.woff) format('woff');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* devanagari */
@font-face {
  font-family: 'Poppins';
  font-style: normal;
  font-weight: 100;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiGyp8kv8JHgFVrLPTucXtGOvWDSHFF.woff) format('woff');
  unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
}
/* latin-ext */
@font-face {
  font-family: 'Poppins';
  font-style: normal;
  font-weight: 100;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiGyp8kv8JHgFVrLPTufntGOvWDSHFF.woff) format('woff');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Poppins';
  font-style: normal;
  font-weight: 100;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiGyp8kv8JHgFVrLPTucHtGOvWDSA.woff) format('woff');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* devanagari */
@font-face {
  font-family: 'Poppins';
  font-style: normal;
  font-weight: 200;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLFj_Z11lE92JQEl8qw.woff) format('woff');
  unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
}
/* latin-ext */
@font-face {
  font-family: 'Poppins';
  font-style: normal;
  font-weight: 200;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLFj_Z1JlE92JQEl8qw.woff) format('woff');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Poppins';
  font-style: normal;
  font-weight: 200;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLFj_Z1xlE92JQEk.woff) format('woff');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* devanagari */
@font-face {
  font-family: 'Poppins';
  font-style: normal;
  font-weight: 300;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLDz8Z11lE92JQEl8qw.woff) format('woff');
  unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
}
/* latin-ext */
@font-face {
  font-family: 'Poppins';
  font-style: normal;
  font-weight: 300;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLDz8Z1JlE92JQEl8qw.woff) format('woff');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Poppins';
  font-style: normal;
  font-weight: 300;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLDz8Z1xlE92JQEk.woff) format('woff');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* devanagari */
@font-face {
  font-family: 'Poppins';
  font-style: normal;
  font-weight: 400;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiEyp8kv8JHgFVrJJbedHFHGPezSQ.woff) format('woff');
  unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
}
/* latin-ext */
@font-face {
  font-family: 'Poppins';
  font-style: normal;
  font-weight: 400;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiEyp8kv8JHgFVrJJnedHFHGPezSQ.woff) format('woff');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Poppins';
  font-style: normal;
  font-weight: 400;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiEyp8kv8JHgFVrJJfedHFHGPc.woff) format('woff');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* devanagari */
@font-face {
  font-family: 'Poppins';
  font-style: normal;
  font-weight: 500;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLGT9Z11lE92JQEl8qw.woff) format('woff');
  unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
}
/* latin-ext */
@font-face {
  font-family: 'Poppins';
  font-style: normal;
  font-weight: 500;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLGT9Z1JlE92JQEl8qw.woff) format('woff');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Poppins';
  font-style: normal;
  font-weight: 500;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLGT9Z1xlE92JQEk.woff) format('woff');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* devanagari */
@font-face {
  font-family: 'Poppins';
  font-style: normal;
  font-weight: 600;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLEj6Z11lE92JQEl8qw.woff) format('woff');
  unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
}
/* latin-ext */
@font-face {
  font-family: 'Poppins';
  font-style: normal;
  font-weight: 600;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLEj6Z1JlE92JQEl8qw.woff) format('woff');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Poppins';
  font-style: normal;
  font-weight: 600;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLEj6Z1xlE92JQEk.woff) format('woff');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* devanagari */
@font-face {
  font-family: 'Poppins';
  font-style: normal;
  font-weight: 700;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLCz7Z11lE92JQEl8qw.woff) format('woff');
  unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
}
/* latin-ext */
@font-face {
  font-family: 'Poppins';
  font-style: normal;
  font-weight: 700;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLCz7Z1JlE92JQEl8qw.woff) format('woff');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Poppins';
  font-style: normal;
  font-weight: 700;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLCz7Z1xlE92JQEk.woff) format('woff');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* devanagari */
@font-face {
  font-family: 'Poppins';
  font-style: normal;
  font-weight: 800;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLDD4Z11lE92JQEl8qw.woff) format('woff');
  unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
}
/* latin-ext */
@font-face {
  font-family: 'Poppins';
  font-style: normal;
  font-weight: 800;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLDD4Z1JlE92JQEl8qw.woff) format('woff');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Poppins';
  font-style: normal;
  font-weight: 800;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLDD4Z1xlE92JQEk.woff) format('woff');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
/* devanagari */
@font-face {
  font-family: 'Poppins';
  font-style: normal;
  font-weight: 900;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLBT5Z11lE92JQEl8qw.woff) format('woff');
  unicode-range: U+0900-097F, U+1CD0-1CF6, U+1CF8-1CF9, U+200C-200D, U+20A8, U+20B9, U+25CC, U+A830-A839, U+A8E0-A8FB;
}
/* latin-ext */
@font-face {
  font-family: 'Poppins';
  font-style: normal;
  font-weight: 900;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLBT5Z1JlE92JQEl8qw.woff) format('woff');
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}
/* latin */
@font-face {
  font-family: 'Poppins';
  font-style: normal;
  font-weight: 900;
  font-display: swap;
  src: url(https://dtconsultancy.wpengine.com/wp-content/fonts/poppins/pxiByp8kv8JHgFVrLBT5Z1xlE92JQEk.woff) format('woff');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}</style><style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1498245525230{background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1498245525230{background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1496925504666{padding-right: 0px !important;padding-left: 0px !important;}.vc_custom_1498038524933{background-color: #ffffff !important;}.vc_custom_1498038091102{margin-bottom: 0px !important;}.vc_custom_1498037963436{margin-bottom: 20px !important;}.vc_custom_1496925512065{padding-right: 0px !important;padding-left: 0px !important;}.vc_custom_1498038509398{background-color: #ffffff !important;}.vc_custom_1498037976817{margin-bottom: 20px !important;}.vc_custom_1498038100200{margin-bottom: 0px !important;}.vc_custom_1496925504666{padding-right: 0px !important;padding-left: 0px !important;}.vc_custom_1498038673111{background-color: #ffffff !important;}.vc_custom_1498038109478{margin-bottom: 0px !important;}.vc_custom_1498037991176{margin-bottom: 20px !important;}.vc_custom_1497938035052{background-image: url(https://dtconsultancy.wpengine.com/wp-content/uploads/2017/06/f-4.jpg?id=9426) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1498162472243{padding-right: 0px !important;padding-left: 0px !important;background-color: #2b2b2b !important;}.vc_custom_1496466320566{padding-right: 0px !important;padding-left: 0px !important;}.vc_custom_1608278319310{padding-right: 0px !important;padding-left: 0px !important;}.vc_custom_1608275264978{padding-right: 0px !important;padding-left: 0px !important;}.vc_custom_1493723034309{margin-bottom: 0px !important;}.vc_custom_1496999192869{margin-bottom: 0px !important;}.vc_custom_1496999201986{margin-bottom: 0px !important;}.vc_custom_1496999211005{margin-bottom: 0px !important;}.vc_custom_1497021947759{padding-right: 0px !important;padding-left: 0px !important;}</style><noscript><style> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript></head>