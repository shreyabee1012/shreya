

      <!DOCTYPE html>
<html>
  <head>
    <title>Candidates -Golden Opportunities</title>
    <style>
      /* Global styles */
      * {
        box-sizing: border-box;

      }
      body {
        margin: 0;
        font-family: Arial, sans-serif;
        font-size: 16px;
        line-height: 1.5;
      }
      header {
        background-color: #4d4d4d00;
        color: #fff;
        padding: 10px;
      }
      h1, h2, h3 {
        margin-top: 0;
      }
      img {
        display: block;
        margin: 0 auto;
        max-width: 100%;
      }
      main {
        max-width: 500px;
        margin: 0 auto;                
        padding: 20px;
      }
      ul {
        list-style: none;
        padding: 0;
      }
      li::before {
        content: "• ";
      }
      footer {
        background-color: #333;
        color: #fff;
        padding: 10px;
        text-align: center;
      }
      
      /* Page-specific styles */
      .company-image {
        margin-bottom: 20px;
      }
      .mission-image {
        margin-top: 20px;
        margin-bottom: 20px;
      }
      .team-image {
        margin-top: 20px;
        margin-bottom: 20px;
      }
      .values-image {
        margin-top: 20px;
      }
    </style>
  </head>
  <body>
    <header>
      <h1>Candidates</h1>
    </header>
    <main>
      <h2>Golden Opportunities</h2>
      <p>Welcome to Our Company Name, where we specialize in providing high-quality services to our customers. We are committed to delivering exceptional results to our clients and are proud to have a team of experienced professionals who are dedicated to meeting your needs.</p>
      <img src="templates/download.png" alt="Our Company Name" class="company-image">
      <h3>Our Mission</h3>
      <p>At Our Company Name, our mission is to provide our clients with the best possible services at competitive prices. We believe that by delivering excellent customer service and maintaining strong relationships with our clients, we can create long-term partnerships that benefit everyone involved.</p>
      <img src="mission-image.jpg" alt="Our Mission" class="mission-image">
      <h3>Our Team</h3>
      <p>Our team is made up of highly trained professionals who are experts in their fields. They are passionate about providing the best possible services to our clients and are dedicated to staying up-to-date with the latest industry trends and technologies.</p>
      <img src="templates/download.jpeg" alt="Our Team" class="team-image">
      <h3>Our Values</h3>
      <ul>
        <li>Customer satisfaction</li>
        <li>Integrity</li>
        <li>Commitment to excellence</li>
        <li>Innovation</li>
        <li>Respect for our clients and employees</li>
      </ul>
      <img src="values-image.jpg" alt="Our Values">


      <div id="main">
        <div class="vc_column-inner" style="width:50%;margin:auto;">
        <div class="wpb_wrapper">
            <div class="ult-animation  ult-animate-viewport  ult-no-mobile " data-animate="bounce" data-animation-delay="0.8" data-animation-duration="1" data-animation-iteration="1" style="opacity:1;-webkit-transition-delay: 0.8s; -moz-transition-delay: 0.8s; transition-delay: 0.8s;" data-opacity_start_effect="">
              <div class="dt-sc-title script-with-sub-title animated bounce" style="opacity:1;-webkit-animation-delay:0.8s;-webkit-animation-duration:1s;-webkit-animation-iteration-count:1; -moz-animation-delay:0.8s;-moz-animation-duration:1s;-moz-animation-iteration-count:1; animation-delay:0.8s;animation-duration:1s;animation-iteration-count:1;test">
               
                  <h5><strong></strong></h5>

              </div>
              <div role="form" class="wpcf7 animated bounce" id="wpcf7-f8442-p8439-o1" lang="en-US" dir="ltr" style="opacity:1;-webkit-animation-delay:0.8s;-webkit-animation-duration:1s;-webkit-animation-iteration-count:1; -moz-animation-delay:0.8s;-moz-animation-duration:1s;-moz-animation-iteration-count:1; animation-delay:0.8s;animation-duration:1s;animation-iteration-count:1;test">
                  <div class="screen-reader-response">
                    <ul></ul>
                  </div>
                
              </div>
              </div>
          </div>

                    <div class="wpcf7-response-output" aria-hidden="true"></div>
                  </form>
              </div>
            </div>
        </div>
      </div>
    </form>
   
      
   
         <!-- ** Container ** -->
        
            <!-- **Primary - End** -->        
         </div>
         <!-- **Container - End** -->
      </div>
    </main>
      <!-- **Main - End** -->   
           <!-- **Footer** -->
      
        