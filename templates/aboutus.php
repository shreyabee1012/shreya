
<style>
      .form-container {
        display: flex;
        flex-direction: column;
        align-items: center;
        padding: 20px;
      }

      .form-control {
        margin-bottom: 20px;
        padding: 10px;
        width: 300px;
        font-size: 16px;
        border: 1px solid #ccc;
        border-radius: 5px;
      }

      label {
        font-weight: bold;
        margin-bottom: 10px;
      }

      input[type="submit"] {
        background-color: #4CAF50;
        color: white;
        padding: 12px 20px;
        border: none;
        border-radius: 5px;
        cursor: pointer;
      }

      input[type="submit"]:hover {
        background-color: #3e8e41;
      }
    </style>

     <!-- **Header - End** -->
      </div>
      <!DOCTYPE html>
<html>
  <head>
    <title>About Us -Golden Opportunities</title>
    <style>
      /* Global styles */
      * {
        box-sizing: border-box;

      }
      body {
        margin: 0;
        font-family: Arial, sans-serif;
        font-size: 16px;
        line-height: 1.5;
      }
      header {
        background-color: #4d4d4d00;
        color: #fff;
        padding: 10px;
      }
      h1, h2, h3 {
        margin-top: 0;
      }
      img {
        display: block;
        margin: 0 auto;
        max-width: 100%;
      }
      main {
        max-width: 100% !important;
        margin: 0 auto;                
        padding: 20px;
      }
      ul {
        list-style: none;
        padding: 0;
      }
      li::before {
        content: "• ";
      }
      footer {
        background-color: #333;
        color: #fff;
        padding: 10px;
        text-align: center;
      }
      
      /* Page-specific styles */
      .company-image {
        margin-bottom: 20px;
      }
      .mission-image {
        margin-top: 20px;
        margin-bottom: 20px;
      }
      .team-image {
        margin-top: 20px;
        margin-bottom: 20px;
      }
      .values-image {
        margin-top: 20px;
      }
    </style>
  </head>
  <body>
    <header>
      <section class="main-title-section-wrapper default" style="">
        <div class="container">
            <div class="main-title-section">
                <h1 style="margin-bottom: 50px;">About Us </h1>
            </div>
            <div class="breadcrumb">
            </div>
        </div>
    </section>
      
    </header>
    <main>
      <h2>What we do?</h2>
     

<div style="opacity:1;-webkit-animation-delay:0.5s;-webkit-animation-duration:1s;-webkit-animation-iteration-count:1; -moz-animation-delay:0.5s;-moz-animation-duration:1s;-moz-animation-iteration-count:1; animation-delay:0.5s;animation-duration:1s;animation-iteration-count:1;test">


We cater to job openings and requirements across industries and at all levels. 

Our wide range of services includes permanent hiring, contract staffing, and temporary staffing services, meeting the staffing needs of various businesses. Our processes are transparent, and we maintain high ethical standards, prioritizing integrity, and confidentiality.

At Golden Opportunities, we strive to provide comprehensive solutions to our clients and candidates, making us the go-to recruitment company for many. Our ultimate goal is to help businesses achieve success by providing the best workforce solutions, and candidates in finding their dream jobs, thereby fostering growth and building a better future.

Our dedicated teams specialize in various verticals, enabling us to understand candidate and client needs in-depth and provide tailored solutions to meet their satisfaction.

Our business model is focused on diverse verticals that cover a wide range of industries, including Leadership Hiring, Finance & Accounting, IT-enabled services, SME Hiring, Research & Analytics, Information Technology, Sales & Marketing, Engineering & Manufacturing, and Training, Human Resources, Quality & Other Support Areas. 

We take pride in serving businesses across various horizontals, including Manufacturing, Retail, BFSI, Telecom, Supply Chain, Travel and Hospitality, Utilities, Health Care, Pharma, Information Technology, Software, E-comm, FMCG, Oil and Gas, and Services.

We possess large databases, further sub-classified or categorized, to meet our client’s needs in a very short time span. Our team of professionals specializes in these verticals and has the expertise to give you validated apt profiles in a shorter period.

We believe in providing impeccable service to our clients to help them find the perfect candidate that fits their job requirements. Additionally, we offer candidates a platform to find their desired job opportunities, pursuing their professional goals with ease.

Experience the best recruitment solutions with Golden Opportunities, where we provide opportunities, nurture growth and create success stories.

</p>
      </div>

      <div id="main">
        <div class="vc_column-inner" style="width:50%;margin:auto;">
        <div class="wpb_wrapper">
            <div class="ult-animation  ult-animate-viewport  ult-no-mobile " data-animate="bounce" data-animation-delay="0.8" data-animation-duration="1" data-animation-iteration="1" style="opacity:1;-webkit-transition-delay: 0.8s; -moz-transition-delay: 0.8s; transition-delay: 0.8s;" data-opacity_start_effect="">
              <div class="dt-sc-title script-with-sub-title animated bounce" style="opacity:1;-webkit-animation-delay:0.8s;-webkit-animation-duration:1s;-webkit-animation-iteration-count:1; -moz-animation-delay:0.8s;-moz-animation-duration:1s;-moz-animation-iteration-count:1; animation-delay:0.8s;animation-duration:1s;animation-iteration-count:1;test">
               
                  <h5><strong></strong></h5>

              </div>
              <div role="form" class="wpcf7 animated bounce" id="wpcf7-f8442-p8439-o1" lang="en-US" dir="ltr" style="opacity:1;-webkit-animation-delay:0.8s;-webkit-animation-duration:1s;-webkit-animation-iteration-count:1; -moz-animation-delay:0.8s;-moz-animation-duration:1s;-moz-animation-iteration-count:1; animation-delay:0.8s;animation-duration:1s;animation-iteration-count:1;test">
                  <div class="screen-reader-response">
                    <ul></ul>
                  </div>
                
              </div>
              </div>
          </div>

             </main>
                    <div class="wpcf7-response-output" aria-hidden="true"></div>
                  </form>
              </div>
            </div>
        </div>
      </div>
    </form>
   
      
   
         <!-- ** Container ** -->
        
            <!-- **Primary - End** -->        
         </div>
         <!-- **Container - End** -->
      </div>
    </main>
      <!-- **Main - End** -->   
           <!-- **Footer** -->
