<footer id="footer">
         <div class="footer-widgets dt-sc-dark-bg">
                        <div class="container">
               <div class='column dt-sc-one-fourth first'>
                  <aside id="text-3" class="widget widget_text">
                     <div class="textwidget">
                        <div  class="wpb_single_image wpb_content_element vc_align_left">
                           <figure class="wpb_wrapper vc_figure">
                              <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="100" height="50" src="templates/R2.png" class="vc_single_image-img attachment-full" alt="" decoding="async" loading="lazy" srcset="templates/R2.png" sizes="(max-width: 200px) 200vw, 200px" /></div>
                           </figure>
                        </div>
                        <p>Consultancy theme is the essence of business themes. We recommend a winning generic strategy based on our insights &amp; experience in diverse businesses.</p>
                        <p>
                        <div class="vc_empty_space"   style="height: 16px"><span class="vc_empty_space_inner"></span></div>
                        <div class='alignleft  dt-sc-subtitle-text' >
                           STAY CONNECTED
                           <div class='dt-sc-hr-invisible-xsmall '> </div>
                        </div>
                        <ul class='dt-sc-sociable  alignleft'>
                           <li class="delicious"><a target="_blank" href="#" rel="noopener"></a></li>
                           <li class="dribbble"><a target="_blank" href="#" rel="noopener"></a></li>
                           <li class="facebook"><a target="_blank" href="#" rel="noopener"></a></li>
                        </ul>
                        </p>
                     </div>
                  </aside>
               </div>
               <div class='column dt-sc-one-fourth '>
                  <aside id="recent-posts-4" class="widget widget_recent_entries">
                     <h3 class="widgettitle">Recent Posts</h3>
                     <ul>
                        <li>
                           <a href="https://dtconsultancy.wpengine.com/the-impact-of-music-on-our-well-being/">The impact of music on our well being</a>
                           <span class="post-date">June 15, 2019</span>
                        </li>
                        <li>
                           <a href="https://dtconsultancy.wpengine.com/golf-clubs-are-for-making-business-deals/">Golf clubs are for making business deals</a>
                           <span class="post-date">June 15, 2017</span>
                        </li>
                        <li>
                           <a href="https://dtconsultancy.wpengine.com/the-accelerator-coaching-program-for-freshers/">The Accelerator Coaching Program for Freshers</a>
                           <span class="post-date">June 15, 2017</span>
                        </li>
                     </ul>
                  </aside>
               </div>
               <div class='column dt-sc-one-fourth '>
                  <aside id="text-4" class="widget widget_text">
                     <h3 class="widgettitle">Quick Links</h3>
                     <div class="textwidget">
                        <ul class="quick-links">
                           <li> <a title="" href="#"> Home </a> </li>
                           <li> <a title="" href="#"> About us </a> </li>
                           <li> <a title="" href="#"> Projects </a> </li>
                           <li> <a title="" href="#"> Careers </a> </li>
                           <li> <a title="" href="#"> Services</a> </li>
                           <li> <a title="" href="#"> Contact</a> </li>
                        </ul>
                     </div>
                  </aside>
               </div>
               <div class='column dt-sc-one-fourth '>
                  <aside id="text-6" class="widget widget_text">
                     <h3 class="widgettitle">Contact Us</h3>
                     <div class="textwidget">
                        <div class='dt-sc-contact-info  '><span class='zmdi zmdi-home zmdi-hc-fw'> </span>71 Pilgrim Avenue, Chevy Chase, <br>MD 20815</div>
                        <div class='dt-sc-hr-invisible-xsmall '> </div>
                        <div class='dt-sc-contact-info  '><span class='zmdi zmdi-smartphone-ring zmdi-hc-fw'> </span>(226) 906-2721<br>(671) 925-1352</div>
                        <div class='dt-sc-hr-invisible-xsmall '> </div>
                        <div class='dt-sc-contact-info  '><span class='zmdi zmdi-time zmdi-hc-fw'> </span>Mon – Sat 9 am to 8 pm<br>Sun – 10 am to 3 pm</div>
                        <div class='dt-sc-hr-invisible-xsmall '> </div>
                        <div class='dt-sc-contact-info  '><span class='zmdi zmdi-email zmdi-hc-fw'> </span><a href="#"> info@agency.com</a></div>
                     </div>
                  </aside>
               </div>
            </div>
         </div>
         <div class="footer-copyright dt-sc-dark-bg">
            <div class="container">
               <div class="column dt-sc-one-half first ">&copy; 2017 Golden Oppourtunities. All rights reserved.                    </div>
               <div class="column dt-sc-one-half sociable">
                  <ul class="dt-sc-sociable">
                     <li class="facebook"><a target="_blank" href="#"></a></li>
                     <li class="google-plus"><a target="_blank" href="#"></a></li>
                     <li class="delicious"><a target="_blank" href="#"></a></li>
                     <li class="instagram"><a target="_blank" href="#"></a></li>
                     <li class="twitter"><a target="_blank" href="#"></a></li>
                  </ul>
               </div>
            </div>
         </div>
      </footer>