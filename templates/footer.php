<!-- <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script> -->
    
    <script>
        jQuery(function(){
            // alert("s");
                  var userdata = JSON.parse(localStorage.getItem('data'));
                  // console.log(userdata);
                  if(userdata == null){
                    console.log("Not Logged In");
                  }else{
                    console.log(userdata.name +" Logged In"); 
                     jQuery("#sign_in").html(userdata.name); 
                     jQuery("#sign_in").removeAttr('href');

                  }
          });
      </script> 

<script type="text/html" id="wpb-modifications"></script><link rel="stylesheet" property="stylesheet" id="rs-icon-set-fa-icon-css" href="https://dtconsultancy.wpengine.com/wp-content/plugins/revslider/public/assets/fonts/font-awesome/css/font-awesome.css" type="text/css" media="all" />
    <link href="https://fonts.googleapis.com/css?family=Raleway:900%2C100%2C600%7CRoboto:400" rel="stylesheet" property="stylesheet" media="all" type="text/css" >
    
        <script type="text/javascript">
            (function () {
                var c = document.body.className;
                c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
                document.body.className = c;
            })()
        </script>
                <script type="text/javascript">
            if(typeof revslider_showDoubleJqueryError === "undefined") {
                function revslider_showDoubleJqueryError(sliderID) {
                    var err = "<div class='rs_error_message_box'>";
                    err += "<div class='rs_error_message_oops'>Oops...</div>";
                    err += "<div class='rs_error_message_content'>";
                    err += "You have some jquery.js library include that comes after the Slider Revolution files js inclusion.<br>";
                    err += "To fix this, you can:<br>&nbsp;&nbsp;&nbsp; 1. Set 'Module General Options' -> 'Advanced' -> 'jQuery & OutPut Filters' -> 'Put JS to Body' to on";
                    err += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jQuery.js inclusion and remove it";
                    err += "</div>";
                err += "</div>";
                    var slider = document.getElementById(sliderID); slider.innerHTML = err; slider.style.display = "block";
                }
            }
            </script>
    <link rel='stylesheet' id='vc_animate-css-css' href='https://dtconsultancy.wpengine.com/wp-content/plugins/js_composer/assets/lib/bower/animate-css/animate.min.css?ver=6.5.0' type='text/css' media='all' />
    <link rel='stylesheet' id='ult-background-style-css' href='https://dtconsultancy.wpengine.com/wp-content/plugins/Ultimate_VC_Addons/assets/min-css/background-style.min.css?ver=3.19.8' type='text/css' media='all' />
    <script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/plugins/yith-woocommerce-wishlist/assets/js/jquery.selectBox.min.js?ver=1.2.0' id='jquery-selectBox-js'></script>
    <script type='text/javascript' id='jquery-yith-wcwl-js-extra'>
    /* <![CDATA[ */
    var yith_wcwl_l10n = {"ajax_url":"\/wp-admin\/admin-ajax.php","redirect_to_cart":"no","multi_wishlist":"","hide_add_button":"1","enable_ajax_loading":"","ajax_loader_url":"https:\/\/dtconsultancy.wpengine.com\/wp-content\/plugins\/yith-woocommerce-wishlist\/assets\/images\/ajax-loader-alt.svg","remove_from_wishlist_after_add_to_cart":"1","is_wishlist_responsive":"1","time_to_close_prettyphoto":"3000","fragments_index_glue":".","reload_on_found_variation":"1","labels":{"cookie_disabled":"We are sorry, but this feature is available only if cookies on your browser are enabled.","added_to_cart_message":"<div class=\"woocommerce-notices-wrapper\"><div class=\"woocommerce-message\" role=\"alert\">Product added to cart successfully<\/div><\/div>"},"actions":{"add_to_wishlist_action":"add_to_wishlist","remove_from_wishlist_action":"remove_from_wishlist","reload_wishlist_and_adding_elem_action":"reload_wishlist_and_adding_elem","load_mobile_action":"load_mobile","delete_item_action":"delete_item","save_title_action":"save_title","save_privacy_action":"save_privacy","load_fragments":"load_fragments"}};
    /* ]]> */
    </script>
    <script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/plugins/yith-woocommerce-wishlist/assets/js/jquery.yith-wcwl.js?ver=3.0.17' id='jquery-yith-wcwl-js'></script>
    <script type='text/javascript' id='contact-form-7-js-extra'>
    /* <![CDATA[ */
    var wpcf7 = {"apiSettings":{"root":"https:\/\/dtconsultancy.wpengine.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"cached":"1"};
    /* ]]> */
    </script>
    <!-- <script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.3.2' id='contact-form-7-js'></script> -->
    <script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/plugins/designthemes-core-features/shortcodes/js/jquery.tabs.min.js?ver=6.1.1' id='dt-sc-tabs-js'></script>
    <script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/plugins/designthemes-core-features/shortcodes/js/jquery.tipTip.minified.js?ver=6.1.1' id='dt-sc-tiptip-js'></script>
    <script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/plugins/designthemes-core-features/shortcodes/js/jquery.inview.js?ver=6.1.1' id='dt-sc-inview-js'></script>
    <script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/plugins/designthemes-core-features/shortcodes/js/jquery.animateNumber.min.js?ver=6.1.1' id='dt-sc-animatenum-js'></script>
    <script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/plugins/designthemes-core-features/shortcodes/js/jquery.donutchart.js?ver=6.1.1' id='dt-sc-donutchart-js'></script>
    <script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/plugins/designthemes-core-features/shortcodes/js/slick.min.js?ver=6.1.1' id='dt-sc-slick-js'></script>
    <script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/plugins/designthemes-core-features/shortcodes/js/jquery.toggle.click.js?ver=6.1.1' id='dt-sc-toggle-click-js'></script>
    <script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/plugins/designthemes-core-features/shortcodes/js/shortcodes.js?ver=6.1.1' id='dt-sc-script-js'></script>
    <script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/plugins/designthemes-fb-pixel/script.js?ver=6.1.1' id='dt-fbpixel-script-js'></script>
    <script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js?ver=2.1.4' id='js-cookie-js'></script>
    <script type='text/javascript' id='woocommerce-js-extra'>
    /* <![CDATA[ */
    var woocommerce_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%"};
    /* ]]> */
    </script>
    <script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js?ver=4.8.3' id='woocommerce-js'></script>
    <script type='text/javascript' id='wc-cart-fragments-js-extra'>
    /* <![CDATA[ */
    var wc_cart_fragments_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_ffc671082c10fa4e8275a26da53c4474","fragment_name":"wc_fragments_ffc671082c10fa4e8275a26da53c4474","request_timeout":"5000"};
    /* ]]> */
    </script>
    <script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js?ver=4.8.3' id='wc-cart-fragments-js'></script>
    <script type='text/javascript' src='//dtconsultancy.wpengine.com/wp-content/plugins/woocommerce/assets/js/prettyPhoto/jquery.prettyPhoto.min.js?ver=3.1.6' id='prettyPhoto-js'></script>
    <script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/themes/consultancy/framework/js/jquery.ui.totop.min.js?ver=6.1.1' id='jquery-ui-totop-js'></script>
    <script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/themes/consultancy/framework/js/jquery.caroufredsel.js?ver=6.1.1' id='jquery-caroufredsel-js'></script>
    <script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/themes/consultancy/framework/js/jquery.debouncedresize.js?ver=6.1.1' id='jquery-debouncedresize-js'></script>
    <script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/themes/consultancy/framework/js/jquery.prettyphoto.js?ver=6.1.1' id='jquery-prettyphoto-js'></script>
    <script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/themes/consultancy/framework/js/jquery.touchswipe.js?ver=6.1.1' id='jquery-touchswipe-js'></script>
    <script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/themes/consultancy/framework/js/jquery.parallax.js?ver=6.1.1' id='jquery-parallax-js'></script>
    <script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/themes/consultancy/framework/js/jquery.downcount.js?ver=6.1.1' id='jquery-downcount-js'></script>
    <script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/themes/consultancy/framework/js/jquery.nicescroll.js?ver=6.1.1' id='jquery-nicescroll-js'></script>
    <script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/themes/consultancy/framework/js/jquery.bxslider.js?ver=6.1.1' id='jquery-bxslider-js'></script>
    <script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/themes/consultancy/framework/js/jquery.fitvids.js?ver=6.1.1' id='jquery-fitvids-js'></script>
    <script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/themes/consultancy/framework/js/jquery.sticky.js?ver=6.1.1' id='jquery-sticky-js'></script>
    <script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/themes/consultancy/framework/js/jquery.simple-sidebar.js?ver=6.1.1' id='jquery-simple-sidebar-js'></script>
    <script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/themes/consultancy/framework/js/jquery.classie.js?ver=6.1.1' id='jquery-classie-js'></script>
    <script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/themes/consultancy/framework/js/jquery.placeholder.js?ver=6.1.1' id='jquery-placeholder-js'></script>
    <script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/themes/consultancy/framework/js/jquery.visualNav.min.js?ver=6.1.1' id='jquery-visualnav-js'></script>
    <script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/plugins/js_composer/assets/lib/bower/isotope/dist/isotope.pkgd.min.js?ver=6.5.0' id='isotope-js'></script>
    <script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/themes/consultancy/framework/js/magnific/jquery.magnific-popup.min.js?ver=6.1.1' id='consultancy-popup-js-js'></script>
    <script type='text/javascript' id='pace-js-extra'>
    /* <![CDATA[ */
    var paceOptions = {"restartOnRequestAfter":"false","restartOnPushState":"false"};
    /* ]]> */
    </script>
    <script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/themes/consultancy/framework/js/pace.min.js?ver=6.1.1' id='pace-js'></script>
    <script type='text/javascript' id='consultancy-jqcustom-js-extra'>
    /* <![CDATA[ */
    var dttheme_urls = {"theme_base_url":"https:\/\/dtconsultancy.wpengine.com\/wp-content\/themes\/consultancy","framework_base_url":"https:\/\/dtconsultancy.wpengine.com\/wp-content\/themes\/consultancy\/framework\/","ajaxurl":"https:\/\/dtconsultancy.wpengine.com\/wp-admin\/admin-ajax.php","url":"https:\/\/dtconsultancy.wpengine.com","stickynav":"enable","stickyele":".menu-wrapper","isRTL":"","loadingbar":"enable","advOptions":"Show Advanced Options"};
    /* ]]> */
    </script>
    <script type='text/javascript' src='js/custom.js' id='consultancy-jqcustom-js'></script>
    <script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/themes/consultancy/framework/js/jquery.cookie.min.js?ver=6.1.1' id='cookie-js'></script>
    <script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/themes/consultancy/framework/js/controlpanel.js?ver=6.1.1' id='consultancy-jqcpanel-js'></script>
    <script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min.js?ver=6.5.0' id='wpb_composer_front_js-js'></script>
    <script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/plugins/js_composer/assets/lib/vc_waypoints/vc-waypoints.min.js?ver=6.5.0' id='vc_waypoints-js'></script>
    <script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/plugins/Ultimate_VC_Addons/assets/min-js/ultimate_bg.min.js?ver=3.19.8' id='ultimate-row-bg-js'></script>
    <script type='text/javascript' src='https://dtconsultancy.wpengine.com/wp-content/plugins/Ultimate_VC_Addons/assets/min-js/vhparallax.min.js?ver=3.19.8' id='jquery.vhparallax-js'></script>


    </body>
    </html>