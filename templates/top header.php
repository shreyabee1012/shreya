<header id="header">
            <!-- **Main Header Wrapper** -->
            <div id="main-header-wrapper" class="main-header-wrapper">
               <div class="container">
                  <!-- **Main Header** -->
                  <div class="main-header">
                     <div id="logo">        
                        <a title="Consultancy WordPress Theme">
                        <img class="normal_logo"  width="100" src="templates/R2.png" alt="Consultancy WordPress Theme" title="Consultancy WordPress Theme" />
                        <img class="darkbg_logo" src="https://dtconsultancy.wpengine.com/wp-content/themes/consultancy/images/light-logo.png" alt="Consultancy WordPress Theme" title="Consultancy WordPress Theme" />
                        </a>
                     </div>
                     <div class="header-right">
                        <div class="vc_row wpb_row vc_row-fluid">
                           <div class="wpb_column vc_column_container vc_col-sm-6">
                              <div class="vc_column-inner ">
                                 <div class="wpb_wrapper"></div>
                              </div>
                           </div>
                           <div class="alignleft wpb_column vc_column_container vc_col-sm-3">
                              <div class="vc_column-inner ">
                                 <div class="wpb_wrapper">
                                    <div class='dt-sc-contact-info '> 
                                          
                                       <div>
                                      (226) 906-2721<br>
                                       <a href="mailto:test@somemail.com">test@somemail.com</a>
                                    </div>
                                    </div>
                                 </div>
                            </div>
                           </div>
                           <div class="alignleft wpb_column vc_column_container vc_col-sm-3">
                              <div class="vc_column-inner ">
                                 <div class="wpb_wrapper">
                                    <div class='dt-sc-contact-info  '>
                                       <span class='zmdi zmdi-time zmdi-hc-fw'> 
                                       </span>9 am to 8 pm <br>Monday - Saturday
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div id="menu-wrapper" class="menu-wrapper menu-with-splitter">
                        <div class="dt-menu-toggle" id="dt-menu-toggle">
                           Menu<span class="dt-menu-toggle-icon"></span>
                        </div>
                       <?php include("templates/navbar.php"); ?>
                             </div>
                           </div>
                         </div>
                       </div>
                         </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- **Main Header** -->
         </header>
         