<!DOCTYPE html>
<html lang="en">
   <head>
      <title>Golden Oppourtunities</title>	
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700&display=swap" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@400;500;600;700;800;900&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="css/animate.css">
      <link rel="stylesheet" href="css/owl.carousel.min.css">
      <link rel="stylesheet" href="css/owl.theme.default.min.css">
      <link rel="stylesheet" href="css/magnific-popup.css">
      <link rel="stylesheet" href="css/flaticon.css">
      <link rel="stylesheet" href="css/style.css">
   </head>
   <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">
      <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar ftco-navbar-light site-navbar-target" id="ftco-navbar">
         <div class="container">
            <img src="templates/R2.png" width="100px;">
            <button class="navbar-toggler js-fh5co-nav-toggle fh5co-nav-toggle" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="oi oi-menu"></span> Menu
            </button>
            <div class="collapse navbar-collapse" id="ftco-nav">
               <ul class="navbar-nav nav ml-auto">
                  <li class="nav-item"><a href="#home-section" class="nav-link"><span>Home Page</span></a></li>
                  <li class="nav-item"><a href="#about-section" class="nav-link"><span>About</span></a></li>
                  <li class="nav-item"><a href="https://gojobs.in" class="nav-link"><span>Jobs</span></a></li>
                  <li class="nav-item"><a href="#" class="nav-link"><span>Knowledge Base</span></a></li>
                  <li class="nav-item"><a href="#contact-section" class="nav-link"><span>Contact Us</span></a></li>
               </ul>
            </div>
         </div>
      </nav>
      <section class="hero-wrap js-fullheight">
         <div class="overlay"></div>
         <div class="container-fluid px-0">
            <div class="row d-md-flex no-gutters slider-text align-items-center js-fullheight justify-content-end">
               <img class="one-third js-fullheight align-self-end order-md-last img-fluid" src="https://preview.colorlib.com/theme/author/images/undraw_book_lover_mkck.svg" alt="" style="margin-top: 64px;" >
               <div class="one-forth d-flex align-items-center ftco-animate js-fullheight">
                  <div class="text mt-5">
                     <h1>Clue Of The Wooden Cottage</h1>
                  </div>
               </div>
            </div>
         </div>
         </section>
         <p style="padding-left: 150px; padding-right: 150px; margin-top: 75px;">Welcome to Golden Opportunities Pvt Ltd, a leading organization with a rich history of 25 + years in providing innovative staffing solutions and recruitment services. Throughout our journey, we have helped numerous organizations and job seekers alike, in finding their ideal fit, and connecting them to a world of opportunities.

         Our vision is simple, we believe in providing high-quality services that are customized to meet the specific needs of our clients. We pride ourselves on our professionalism, expertise, and our unwavering commitment to delivering excellence. Our dedicated team of experts is comprised of talented individuals with diverse backgrounds, skillsets, and experiences. Together, we work tirelessly to identify and address our clients' needs, and to help them achieve their goals.</p>

         <p style="padding-left: 150px; padding-right: 150px; ">At Golden Opportunities Pvt Ltd, we understand that every business is unique, and that the needs of each organization vary. That's why we offer an extensive range of services that can be tailored to our clients' specific needs. From recruitment solutions and staffing services to HR consultancy and outsourcing, we help organizations across various industries find the talent they need to succeed.

         Our team is committed to developing lasting relationships with our clients and candidates, built on trust, transparency, and respect. We strive to deliver exceptional customer service and support, and to create an environment that fosters growth, innovation, and success.<br>
<br>
         We invite you to join us on the journey to success, and to experience the Golden Opportunities difference.

</p>
      
      <section class="ftco-about img ftco-section" id="about-section">
         <div class="container">
            <div class="row d-flex no-gutters">
               <div class="col-md-6 col-lg-6 d-flex">
                  <div class="img-about img d-flex align-items-stretch" style="margin-top: 10px;">
                     <div class="overlay"></div>
                     <div class="img d-flex align-self-stretch align-items-center" style="background-image:url(https://preview.colorlib.com/theme/author/images/bg_1.jpg);"></div>
                  </div>
               </div>
               <div class="col-md-6 col-lg-6 pl-md-5">
                  <div class="row justify-content-start pb-3">
                     <div class="col-md-12 heading-section ftco-animate">
                        <h2 style="margin-top: -100px; margin-left: -200px; ">What We Do</h2>
                        <br>
                        <p>We cater to job openings and requirements across industries and at all levels. 
                           Our wide range of services includes permanent hiring, contract staffing, and temporary staffing services, meeting the staffing needs of various businesses. Our processes are transparent, and we maintain high ethical standards, prioritizing integrity, and confidentiality.
                        </p>
                        <div class="text-about">
                           <p>At Golden Opportunities, we strive to provide comprehensive solutions to our clients and candidates, making us the go-to recruitment company for many. Our ultimate goal is to help businesses achieve success by providing the best workforce solutions, and candidates in finding their dream jobs, thereby fostering growth and building a better future.
                              Our dedicated teams specialize in various verticals, enabling us to understand candidate and client needs in-depth and provide tailored solutions to meet their satisfaction.
                           </p>
                           <p>Our business model is focused on diverse verticals that cover a wide range of industries, including Leadership Hiring, Finance & Accounting, IT-enabled services, SME Hiring, Research & Analytics, Information Technology, Sales & Marketing, Engineering & Manufacturing, and Training, Human Resources, Quality & Other Support Areas. 
                              We take pride in serving businesses across various horizontals, including Manufacturing, Retail, BFSI, Telecom, Supply Chain, Travel and Hospitality, Utilities, Health Care, Pharma, Information Technology, Software, E-comm, FMCG, Oil and Gas, and Services.
                           </p>
                           <p>We possess large databases, further sub-classified or categorized, to meet our client’s needs in a very short time span. Our team of professionals specializes in these verticals and has the expertise to give you validated apt profiles in a shorter period.
                              We believe in providing impeccable service to our clients to help them find the perfect candidate that fits their job requirements. Additionally, we offer candidates a platform to find their desired job opportunities, pursuing their professional goals with ease.
                              Experience the best recruitment solutions with Golden Opportunities, where we provide opportunities, nurture growth and create success stories

                           </p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <section style="display: none;">
      	<?php
    include("js/blogs.php");
?>
 </section>
<section class="ftco-section testimony-section ftco-no-pb" id="testimonial-section" style="display: none;">
         <div class="img img-bg border" style="background-image: url(https://preview.colorlib.com/theme/author/images/bg_4.jpg);"></div>
         <div class="overlay"></div>
         <div class="container">
            <div class="row justify-content-center mb-5">
               <div class="col-md-12 text-center heading-section heading-section-white ftco-animate">
                  <span class="subheading">Testimonial</span>
                  <h2 class="mb-3">Kinds Words From Customers</h2>
               </div>
            </div>
            <div class="row ftco-animate">
               <div class="col-md-12">
                  <div class="carousel-testimony owl-carousel ftco-owl">
                    <div class="item">
                        <div class="testimony-wrap py-4">
                           <div class="icon d-flex align-items-center justify-content-center">
                              <span class="fa fa-quote-left">
                           </div>
                           <div class="text">
                           <p class="mb-4">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                       	    <div class="d-flex align-items-center">
                           
                           <div class="pl-3">
                           
                           </div>
                           </div>
                           </div>
                        </div>
                     </div>
                     <div class="item">
                     <div class="testimony-wrap py-4">
                     <div class="icon d-flex align-items-center justify-content-center"><span class="fa fa-quote-left"></div>
                     <div class="text">
                     <p class="mb-4">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                     <div class="d-flex align-items-center">
                     <div class="user-img" style="background-image: url(https://preview.colorlib.com/theme/author/images/person_2.jpg)"></div>
                     <div class="pl-3">
                     <p class="name">Roger Scott</p>
                     <span class="position">Marketing Manager</span>
                     </div>
                     </div>
                     </div>
                     </div>
                     </div>
                     <div class="item">
                     <div class="testimony-wrap py-4">
                     <div class="icon d-flex align-items-center justify-content-center"><span class="fa fa-quote-left"></div>
                     <div class="text">
                     <p class="mb-4">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                     <div class="d-flex align-items-center">
                     <div class="user-img" style="background-image: url(https://preview.colorlib.com/theme/author/images/person_3.jpg)"></div>
                     <div class="pl-3">
                     <p class="name">Roger Scott</p>
                     <span class="position">Marketing Manager</span>
                     </div>
                     </div>
                     </div>
                     </div>
                     </div>
                     <div class="item">
                     <div class="testimony-wrap py-4">
                     <div class="icon d-flex align-items-center justify-content-center"><span class="fa fa-quote-left"></div>
                     <div class="text">
                     <p class="mb-4">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                     <div class="d-flex align-items-center">
                     <div class="user-img" style="background-image: url(https://preview.colorlib.com/theme/author/images/person_1.jpg)"></div>
                     <div class="pl-3">
                     <p class="name">Roger Scott</p>
                     <span class="position">Marketing Manager</span>
                     </div>
                     </div>
                     </div>
                     </div>
                     </div>
                     <div class="item">
                     <div class="testimony-wrap py-4">
                     <div class="icon d-flex align-items-center justify-content-center"><span class="fa fa-quote-left"></div>
                     <div class="text">
                     <p class="mb-4">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                     <div class="d-flex align-items-center">
                     <div class="user-img" style="background-image: url(https://preview.colorlib.com/theme/author/images/person_2.jpg)"></div>
                     <div class="pl-3">
                     <p class="name">Roger Scott</p>
                     <span class="position">Marketing Manager</span>
                     </div>
                     </div>
                     </div>
                     </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      
      <section class="ftco-about img ftco-section ftco-no-pt ftco-no-pb" id="author-section" style="margin-top: 50px; display: none;">

      <div class="container">
      <div class="row d-flex no-gutters">
      <div class="col-md-6 col-lg-6 d-flex">
      <div class="img-about img d-flex align-items-stretch">
      <div class="overlay"></div>
      <div class="img d-flex align-self-stretch align-items-center">
      	<img src="templates/joshua.jpg" height="100%">
      </div>
      </div>
      </div>
      <div class="col-md-6 col-lg-6 d-flex">
      <div class="py-md-5 w-100 bg-light px-md-5">
      <div class="py-md-5">
      <div class="row justify-content-start pb-3">
      <div class="col-md-12 heading-section ftco-animate">
      <span class="subheading">Know More About The Author</span>
      <h2 class="mb-4">Joshua</h2>
      <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
      <ul class="about-info mt-4 px-md-0 px-2">
      <li class="d-flex"><span>Name:</span> <span>Franklin Henderson</span></li>
      <li class="d-flex"><span>Date of birth:</span> <span>November 28, 1980</span></li>
      <li class="d-flex"><span>Address:</span> <span>San Francisco CA 97987 USA</span></li>
      <li class="d-flex"><span>Zip code:</span> <span>1000</span></li>
      <li class="d-flex"><span>Email:</span> <span><a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="b5d3c7d4dbded9dcdbc6d4d8c5d9d0f5d2d8d4dcd99bd6dad8">[email&#160;protected]</a></span></li>
      <li class="d-flex"><span>Phone: </span> <span>+1-2234-5678-9-0</span></li>
      </ul>
      </div>
      </div>
      <div class="counter-wrap ftco-animate d-flex mt-md-3">
      <div class="text">
      <p class="mb-4 btn-custom">
      <span class="number" data-number="120">0</span>
            </p>
      </div>
      </div>
      </div>
      </div>
      </div>
      </div>
      </div>
      </section>


      <section class="ftco-section contact-section ftco-no-pb" id="contact-section">
      <div class="container">
      <div class="row justify-content-center mb-5 pb-3">
      <div class="col-md-7 heading-section text-center ftco-animate">
      <h2 class="mb-4">Contact Me</h2>
      <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia</p>
      </div>
      </div>
      <div class="row d-flex contact-info mb-5">
      <div class="col-md-6 col-lg-3 d-flex ftco-animate">
      <div class="align-self-stretch box text-center p-4 bg-light">
      <div class="icon d-flex align-items-center justify-content-center">
      <span class="fa fa-map-marker"></span>
      </div>
      <div>
      <h3 class="mb-4">Address</h3>
      <p> V6, Old No V43, 12th Street, V Block, Anna Nagar, Chennai, Tamil Nadu 600040</p>
      </div>
      </div>
      </div>
      <div class="col-md-6 col-lg-3 d-flex ftco-animate">
      <div class="align-self-stretch box text-center p-4 bg-light">
      <div class="icon d-flex align-items-center justify-content-center">
      <span class="fa fa-phone"></span>
      </div>
      <div>
      <h3 class="mb-4">Contact Number</h3>
      <p><a href="tel://1234567920">+ 1235 2355 98</a></p>
      </div>
      </div>
      </div>
      <div class="col-md-6 col-lg-3 d-flex ftco-animate">
      <div class="align-self-stretch box text-center p-4 bg-light">
      <div class="icon d-flex align-items-center justify-content-center">
      <span class="fa fa-paper-plane"></span>
      </div>
      <div>
      <h3 class="mb-4">Email Address</h3>
      <p><a href="/cdn-cgi/l/email-protection#244d4a424b645d4b5156574d50410a474b49"><span class="__cf_email__" data-cfemail="731a1d151c330a1c0601001a07165d101c1e">[email&#160;protected]</span></a></p>
      </div>
      </div>
      </div>
      <div class="col-md-6 col-lg-3 d-flex ftco-animate">
      <div class="align-self-stretch box text-center p-4 bg-light">
      <div class="icon d-flex align-items-center justify-content-center">
      <span class="fa fa-globe"></span>
      </div>
      <div>
      <h3 class="mb-4">Website</h3>
      <p><a href="#">gojobs.com</a></p>
      </div>
      </div>
      </div>
      </div>
      <div class="row no-gutters block-9">
      <div class="col-md-6 order-md-last d-flex">
      <form action="#" class="bg-light p-4 p-md-5 contact-form">
      <div class="form-group">
      <input type="text" class="form-control" placeholder="Your Name">
      </div>
      <div class="form-group">
      <input type="text" class="form-control" placeholder="Your Email">
      </div>
      <div class="form-group">
      <input type="text" class="form-control" placeholder="Subject">
      </div>
      <div class="form-group">
      <textarea name="" id="" cols="30" rows="7" class="form-control" placeholder="Message"></textarea>
      </div>
      <div class="form-group">
      <input type="submit" value="Send Message" class="btn btn-primary py-3 px-5">
      </div>
      </form>
      </div>
      <div class="col-md-6 d-flex">
      <div id="map" class="map"></div>
      </div>
      </div>
      </div>
      </section>
      <footer class="ftco-footer ftco-section">
      <div class="container">
      <div class="row mb-5">
      <div class="col-md">
      <div class="ftco-footer-widget mb-4">
      <h2 class="ftco-heading-2">About</h2>
      <p>Welcome to Golden Opportunities Pvt Ltd, a leading organization with a rich history of 25 + years in providing innovative staffing solutions and recruitment services. Throughout our journey, we have helped numerous organizations and job seekers alike, in finding their ideal fit, and connecting them to a world of opportunities.</p>
      <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
      <li class="ftco-animate"><a href="#"><span class="fa fa-twitter"></span></a></li>
      <li class="ftco-animate"><a href="#"><span class="fa fa-facebook"></span></a></li>
      <li class="ftco-animate"><a href="#"><span class="fa fa-instagram"></span></a></li>
      </ul>
      </div>
      </div>
      <div class="col-md">
      <div class="ftco-footer-widget mb-4 ml-md-4">
      <h2 class="ftco-heading-2">Links</h2>
      <ul class="list-unstyled">
      <li><a href="#home-section"></span>Home Page</a></li>
      <li><a href="#about-section"></span>What we do</a></li>
      <li><a href="https://gojobs.in"></span>Jobs</a></li>
      <li><a href="#"></span>Knowledge Base</a></li>
      <li><a href="#contact-section"></span>Contact Us</a></li>
      </ul>
      </div>
      </div>
      <div class="col-md">
      <div class="ftco-footer-widget mb-4">
      <h2 class="ftco-heading-2">Services</h2>
      <ul class="list-unstyled">
      <li><a href="#"></span>Experience</a></li>
      <li><a href="#"></span>Marketing Goals</a></li>
      <li><a href="#"></span>Targetting Vission</a></li>
      </ul>
      </div>
      </div>
      <div class="col-md">
      <div class="ftco-footer-widget mb-4">
      <h2 class="ftco-heading-2">Have a Questions?</h2>
      <div class="block-23 mb-3">
      <ul>
      <li><span class="icon fa fa-map-marker"></span><span class="text"> V6, Old No V43,12th Street, V Block, Anna Nagar, Chennai, Tamil Nadu 600040</span></li>
      <li><a href="#"><span class="icon fa fa-phone"></span><span class="text">044 4347 0000</span></a></li>
      <li><a href="#"><span class="icon fa fa-paper-plane"></span><span class="text"><span class="__cf_email__" data-cfemail="056c6b636a457c6a7077616a68646c6b2b666a68">[email&#160;protected]</span></span></a></li>
      </ul>
      </div>
      </div>
      </div>
      </div>
      <div class="row">
      <div class="col-md-12 text-center">
      <p>
      Copyright &copy;<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
      </p>
      </div>
      </div>
      </div>
      </footer>
      <div id="ftco-loader" class="show fullscreen">
         <svg class="circular" width="48px" height="48px">
            <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
            <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00" />
         </svg>
      </div>
      <script src="js/jquery.min.js"></script>
      <script src="js/jquery-migrate-3.0.1.min.js"></script>
      <script src="js/popper.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/jquery.easing.1.3.js"></script>
      <script src="js/jquery.waypoints.min.js"></script>
      <script src="js/jquery.stellar.min.js"></script>
      <script src="js/owl.carousel.min.js"></script>
      <script src="js/jquery.magnific-popup.min.js"></script>
      <script src="js/jquery.animateNumber.min.js"></script>
      <script src="js/scrollax.min.js"></script>
      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
      <script src="js/google-map.js"></script>
      <script src="js/main.js"></script>
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
      <script>
         window.dataLayer = window.dataLayer || [];
         function gtag(){dataLayer.push(arguments);}
         gtag('js', new Date());
         
         gtag('config', 'UA-23581568-13');
      </script>
      <script defer src="https://static.cloudflareinsights.com/beacon.min.js/v52afc6f149f6479b8c77fa569edb01181681764108816" integrity="sha512-jGCTpDpBAYDGNYR5ztKt4BQPGef1P0giN6ZGVUi835kFF88FOmmn8jBQWNgrNd8g/Yu421NdgWhwQoaOPFflDw==" data-cf-beacon='{"rayId":"7bcdf35cad17f480","token":"cd0b4b3a733644fc843ef0b185f98241","version":"2023.3.0","si":100}' crossorigin="anonymous"></script>
   </body>
</html>

